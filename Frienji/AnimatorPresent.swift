//
//  AnimatorPresent.swift
//  Frienji
//
//  Created by bolek on 27.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class AnimatorPresent: NSObject, UIViewControllerAnimatedTransitioning
{
    let duration : Double = 0.25;
    var isPresenting : Bool = false;
    
    override init()
    {
        super.init();
    }
    
    //MARK: UIViewControllerAnimatedTransitioning
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval
    {
        return self.duration;
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning)
    {
        if(self.isPresenting == true)
        {
            self.presentAnimation(transitionContext);
        }
        else
        {
            self.dismissAnimation(transitionContext);
        }
    }
    
    //MARK: Private methods
    
    private func presentAnimation(transitionContext: UIViewControllerContextTransitioning)
    {
        let container : UIView = transitionContext.containerView() as UIView!;
        container.backgroundColor = UIColor.clearColor();
        
        let viewFrom : UIView = transitionContext.viewForKey(UITransitionContextFromViewKey) as UIView!;
        viewFrom.frame = container.frame;
        container.addSubview(viewFrom);
        
        let viewTo : UIView = transitionContext.viewForKey(UITransitionContextToViewKey) as UIView!;
        viewTo.frame = container.frame;
        container.addSubview(viewTo);
        
        let viewControllerTo = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as UIViewController!;
        let classTo : AnyClass;
        
        if(viewControllerTo.isKindOfClass(UINavigationController.classForCoder()))
        {
            classTo = (viewControllerTo as! UINavigationController).topViewController!.classForCoder;
        }
        else
        {
            classTo = viewControllerTo!.classForCoder;
        }
        
        if(false) //TODO: - rewrite coach mark animation
        {
            /*
             if([classTo isSubclassOfClass:[CoachMarkViewController class]])
             {
             viewTo.alpha = 0.0f;
             viewTo.transform = CGAffineTransformMakeScale(1.4f, 1.4f);
             [UIView animateWithDuration:_duration delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^
             {
             viewTo.alpha = 1.0f;
             viewTo.transform = CGAffineTransformIdentity;
             }
             completion:^(BOOL finished)
             {
             [transitionContext completeTransition:YES];
             }];
             return;
             }
             */
            return;
        }
        
        viewControllerTo.view.frame = CGRectMake(0.0, viewControllerTo.view.frame.size.height, viewControllerTo.view.frame.size.width, viewControllerTo.view.frame.size.height);
        
        UIView.animateWithDuration(self.duration, animations: {
            viewControllerTo.view.frame = viewControllerTo.view.bounds;
            }) { (finished) in
                transitionContext.completeTransition(true);
        };
    }
    
    private func dismissAnimation(transitionContext: UIViewControllerContextTransitioning)
    {
        let container : UIView = transitionContext.containerView() as UIView!;
        container.backgroundColor = UIColor.clearColor();
        
        let viewTo : UIView = transitionContext.viewForKey(UITransitionContextToViewKey) as UIView!;
        viewTo.frame = container.frame;
        container.addSubview(viewTo);
        
        let viewFrom : UIView = transitionContext.viewForKey(UITransitionContextFromViewKey) as UIView!;
        viewFrom.frame = container.frame;
        container.addSubview(viewFrom);
        
        let viewControllerFrom = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as UIViewController!;
        let classTo : AnyClass;
        
        if(viewControllerFrom.isKindOfClass(UINavigationController.classForCoder()))
        {
            classTo = (viewControllerFrom as! UINavigationController).topViewController!.classForCoder;
        }
        else
        {
            classTo = viewControllerFrom!.classForCoder;
        }
        
        if(false) //TODO: - rewrite coach mark animation
        {
            /*
             if([classFrom isSubclassOfClass:[CoachMarkViewController class]])
             {
             CABasicAnimation *animationScale = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
             animationScale.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
             animationScale.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.4f, 1.4f, 1.0)];
             
             CABasicAnimation *animationOpacity = [CABasicAnimation animationWithKeyPath:@"opacity"];
             animationOpacity.fromValue = @(1.0f);
             animationOpacity.toValue = @(0.0f);
             
             CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
             animationGroup.animations = @[animationScale, animationOpacity];
             animationGroup.duration = _duration;
             animationGroup.fillMode = kCAFillModeForwards;
             animationGroup.removedOnCompletion = NO;
             animationGroup.delegate = self;
             animationGroup.timingFunction =  [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
             [animationGroup setValue:@"dismiss" forKey:@"name"];
             [animationGroup setValue:transitionContext forKey:@"transitionContext"];
             [animationGroup setValue:viewFrom forKey:@"view"];
             [viewFrom.layer addAnimation:animationGroup forKey:nil];
             
             return;
             }
             */
            return;
        }
        
        UIView.animateWithDuration(self.duration, animations: {
            viewControllerFrom.view.frame = CGRectMake(0.0, viewControllerFrom.view.frame.size.height, viewControllerFrom.view.frame.size.width, viewControllerFrom.view.frame.size.height);
        }) { (finished) in
            transitionContext.completeTransition(true);
        };
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool)
    {
        let animationName = anim.valueForKey("name") as! String;
        
        if(animationName == "dismiss")
        {
            let view = anim.valueForKey("view")!;
            view.layer.removeAllAnimations();
            let transitionContext = anim.valueForKey("transitionContext")!;
            transitionContext.completeTransition(true);
        }
    }
}
