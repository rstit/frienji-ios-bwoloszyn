//
//  AnimatorPush.swift
//  Frienji
//
//  Created by bolek on 27.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class AnimatorPush: NSObject, UIViewControllerAnimatedTransitioning {

    private let duration :Double = 0.2;
    var isPresenting :Bool = false;
    
    override init()
    {
        super.init();
    }
    
    //MARK: UIViewControllerAnimatedTransitioning
    
    func transitionDuration(transitionContext :UIViewControllerContextTransitioning?) -> NSTimeInterval
    {
        return self.duration;
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning)
    {
        if(self.isPresenting == true)
        {
            self.pushAnimation(transitionContext);
        }
        else
        {
            self.popAnimation(transitionContext);
        }
    }
    
    //MARK: Private methods
    
    func pushAnimation(transitionContext :UIViewControllerContextTransitioning)
    {
        let inView :UIView = transitionContext.containerView()!;
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!;
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!;
        
        fromViewController.view.frame = inView.bounds;
        toViewController.view.frame = inView.bounds;
        
        inView.addSubview(fromViewController.view);
        inView.addSubview(toViewController.view);
        
        let toViewControllerActual : UIViewController;
        if(toViewController.isKindOfClass(UINavigationController.classForCoder()))
        {
            toViewControllerActual = (toViewController as! UINavigationController).topViewController!;
        }
        else
        {
            toViewControllerActual = toViewController;
        }
        let toClass :AnyClass = toViewControllerActual.classForCoder;
        
        let fromViewControllerActual : UIViewController;
        if(fromViewController.isKindOfClass(UINavigationController.classForCoder()))
        {
            fromViewControllerActual = (fromViewController as! UINavigationController).topViewController!;
        }
        else
        {
            fromViewControllerActual = fromViewController;
        }
        let fromClass :AnyClass = fromViewControllerActual.classForCoder;
        
        if(fromClass.isSubclassOfClass(ArViewController.classForCoder()) && toClass.isSubclassOfClass(ZooViewController.classForCoder()))
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: true, transitionContext: transitionContext);
        }
            //TODO: - it wont be newsfeed, probobly it will be profile, handle further views so that they all arrive from the same sime
        else if(fromClass.isSubclassOfClass(ArViewController.classForCoder()) && toClass.isSubclassOfClass(NewsfeedViewController.classForCoder()))
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: false, transitionContext: transitionContext);
        }
        else if(false) //TODO: - handle some animations as Modal - that is from bottom
        {
            self.showModalFromViewController(fromViewController, toViewController: toViewController, transitionContext: transitionContext);
        }
        else
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: true, transitionContext: transitionContext);
        }
    }
    
    func popAnimation(transitionContext :UIViewControllerContextTransitioning)
    {
        let inView :UIView = transitionContext.containerView()!;
        inView.backgroundColor = UIColor.clearColor();
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!;
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!;
        
        fromViewController.view.frame = inView.bounds;
        toViewController.view.frame = inView.bounds;
        
        inView.addSubview(fromViewController.view);
        inView.addSubview(toViewController.view);
        
        let toViewControllerActual : UIViewController;
        if(toViewController.isKindOfClass(UINavigationController.classForCoder()))
        {
            toViewControllerActual = (toViewController as! UINavigationController).topViewController!;
        }
        else
        {
            toViewControllerActual = toViewController;
        }
        let toClass :AnyClass = toViewControllerActual.classForCoder;
        
        let fromViewControllerActual : UIViewController;
        if(fromViewController.isKindOfClass(UINavigationController.classForCoder()))
        {
            fromViewControllerActual = (fromViewController as! UINavigationController).topViewController!;
        }
        else
        {
            fromViewControllerActual = fromViewController;
        }
        let fromClass :AnyClass = fromViewControllerActual.classForCoder;
        
        //TODO: - handle other transitions like fin push method
        if(fromClass.isSubclassOfClass(ZooViewController.classForCoder()) && toClass.isSubclassOfClass(ArViewController.classForCoder()))
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: false, transitionContext: transitionContext);
        }
        else if(fromClass.isSubclassOfClass(NewsfeedViewController.classForCoder()) && toClass.isSubclassOfClass(ArViewController.classForCoder()))
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: true, transitionContext: transitionContext);
        }
        else if(false) //TODO: - handle some dismissing animations as Modal - that is from bottom
        {
            self.dismissModalFromViewController(fromViewController, toViewController: toViewController, transitionContext: transitionContext);
        }
        else
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: false, transitionContext: transitionContext);
        }
    }
    
    func dismissModalFromViewController(fromViewController :UIViewController, toViewController :UIViewController, transitionContext :UIViewControllerContextTransitioning)
    {
        let inView = transitionContext.containerView()!;
        inView.bringSubviewToFront(fromViewController.view);
        
        UIView.animateWithDuration(self.duration, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {
            fromViewController.view.frame = CGRectMake(0.0, inView.frame.size.height, fromViewController.view.frame.size.width, fromViewController.view.frame.size.height);
            }) { (finished) in
                transitionContext.completeTransition(true);
        };
    }
    
    func showModalFromViewController(fromViewController :UIViewController, toViewController :UIViewController, transitionContext :UIViewControllerContextTransitioning)
    {
        let inView = transitionContext.containerView()!;
        toViewController.view.frame = CGRectMake(0.0, inView.frame.size.height, toViewController.view.frame.size.width, toViewController.view.frame.size.height);
        
        UIView.animateWithDuration(self.duration, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {
            toViewController.view.frame = CGRectMake(100, 100, toViewController.view.frame.size.width-100, toViewController.view.frame.size.height-100);
            }) { (finished) in
                transitionContext.completeTransition(true);
        }
    }
    
    func animateFromViewController(fromViewController :UIViewController, toViewController :UIViewController, towardLeft :Bool, transitionContext :UIViewControllerContextTransitioning)
    {
        if(towardLeft)
        {
            toViewController.view.frame = CGRectMake(toViewController.view.frame.width, 0.0, toViewController.view.frame.size.width, toViewController.view.frame.size.height);
        }
        else
        {
            toViewController.view.frame = CGRectMake(-toViewController.view.frame.width, 0.0, toViewController.view.frame.size.width, toViewController.view.frame.size.height);
        }
        
        UIView.animateWithDuration(self.duration, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {
            toViewController.view.frame = toViewController.view.bounds;
            if(towardLeft)
            {
                fromViewController.view.frame = CGRectMake(0, 0.0, fromViewController.view.frame.size.width, fromViewController.view.frame.size.height);
            }
            else
            {
                fromViewController.view.frame = CGRectMake(0, 0.0, fromViewController.view.frame.size.width, fromViewController.view.frame.size.height);
            }
        }) { (finished) in
            transitionContext.completeTransition(true);
        };
    }
}
