//
//  AppDelegate.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Parse

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        enableExternalServices()
        
        if(Settings.sharedInstance.userVerified)
        {
            
        }
        else
        {
            
        }
        
        registerForPushNotifications()
        
        //load app structure
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.makeKeyAndVisible()
        
        let arController = ArViewController.loadFromStoryboard()
        let navCont = UINavigationController();
        navCont.viewControllers = [arController];
        navCont.delegate = TransitionDelegate.sharedInstance;
        
        
//        let mapController = MapViewController.loadFromStoryboard()
//        
//        let overviewController = OverviewViewController.loadFromStoryboard()
//        overviewController.configureWithBottomController(mapController, overviewController: arController)
//        
//        let navigationController = TracesNavigationController(navigationBarClass: TracesNavigationBar.self, toolbarClass: UIToolbar.self)
//        navigationController.setNavigationBarHidden(true, animated: false)
//        navigationController.viewControllers = [overviewController]
        
        self.window?.rootViewController = navCont;
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK: Open URL
    
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool
    {
        return false
    }
    
    // MARK: Push notifications
    
    private func registerForPushNotifications() {
        
        let types:UIUserNotificationType = ([.Alert, .Badge, .Sound])
        let settings:UIUserNotificationSettings = UIUserNotificationSettings(forTypes: types, categories: nil)
        
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        let characterSet: NSCharacterSet = NSCharacterSet(charactersInString: "<>")
        
        let deviceTokenString: String = (deviceToken.description as NSString)
            .stringByTrimmingCharactersInSet(characterSet)
            .stringByReplacingOccurrencesOfString(" ", withString: "") as String
        
        print("device token: \(deviceTokenString), not trimmed: \(deviceToken)")
        
        let oldToken = Settings.sharedInstance.pushNotificationsToken
        if (oldToken != nil && deviceTokenString != oldToken!) || oldToken == nil || !Settings.sharedInstance.registeredForPushNotifications {
            
            //register on the backend
            
            //to do
        
        }
        
        Settings.sharedInstance.pushNotificationsToken = deviceTokenString
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        print("Notification: \(userInfo)")
        
        //to do
    }
    
    // MARK: External services
    
    private func enableExternalServices() {
        
        Parse.enableLocalDatastore();
        Parse.setLogLevel(PFLogLevel.None);
        Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configuration) in
            configuration.applicationId = "F8q0lfRvQSusUOQAZQD34pUCjzdw4uN6lSbKb0N8";
            configuration.clientKey = "kWZqgZV2zzWHVr8tTPFwjksUQ8OU9LU32eDF5tSp";
            configuration.server = "http://staging-parse-frienji.herokuapp.com/parse";
        }));
        
        
        //Fabric
        if ConfigurationsHelper.sharedInstance.isRunningInRelease() {
            
            Fabric.with([Crashlytics.self])
            
        }
        
        //Google Maps
        //        GMSServices.provideAPIKey(ConfigurationsHelper.sharedInstance.getGoogleApiKey())
    }
    
    func checkIfUserIsVeried() -> Bool
    {
        return true;
    }

}

