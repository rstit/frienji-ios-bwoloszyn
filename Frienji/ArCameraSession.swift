//
//  ArCameraSession.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import AVFoundation

protocol ArCameraSessionProtocol: class {
    
    func arCameraSession(session:ArCameraSession, didOutputSampleBuffer sampleBuffer: CMSampleBuffer) -> Void
}

class ArCameraSession : NSObject, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    private let kCameraSessionQueueIdentifier = "ArCameraSessionQueue"
    
    private var captureSession:AVCaptureSession!
    private var videoOutput:AVCaptureVideoDataOutput!
    private var backCamera:AVCaptureDevice?
    
    private var cameraSessionQueue: dispatch_queue_t!
    
    weak var delegate:ArCameraSessionProtocol?
    
    // MARK: Init
    
    override init() {
        super.init()
        
        self.captureSession = AVCaptureSession()
        self.captureSession.sessionPreset = AVCaptureSessionPreset1280x720
        
        self.cameraSessionQueue = dispatch_queue_create(kCameraSessionQueueIdentifier, DISPATCH_QUEUE_SERIAL)
        
        configureVideoOutput()
        findAvailableCameras()
    }
    
    // MARK: Permissions
    
    private func checkIfUserHasEnabledCameraPermissions() -> Bool {
        
        let authorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        
        return authorizationStatus == AVAuthorizationStatus.Authorized || authorizationStatus == AVAuthorizationStatus.NotDetermined
    }
    
    // MARK: Setup
    
    private func configureVideoOutput() {
        
        self.videoOutput = AVCaptureVideoDataOutput()
        self.videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey : Int(kCVPixelFormatType_32BGRA)]
        self.videoOutput.alwaysDiscardsLateVideoFrames = true
        
        self.videoOutput.setSampleBufferDelegate(self, queue: self.cameraSessionQueue)
        
        if self.captureSession.canAddOutput(self.videoOutput) {
            
            self.captureSession.addOutput(self.videoOutput)
        }
    }
    
    // MARK: Camera
    
    private func findAvailableCameras() {
        
        let devices = AVCaptureDevice.devices()
        
        for device in devices {
            
            if device.hasMediaType(AVMediaTypeVideo) && device.position == AVCaptureDevicePosition.Back {
                
                self.backCamera = device as? AVCaptureDevice
                break
            }
        }
    }
    
    // MARK: Session
    
    func startCameraSession() {
        
        //back camera as default one
        guard let camera = self.backCamera where self.checkIfUserHasEnabledCameraPermissions() else {
            
            return
        }
        
        do {
            
            let deviceInput = try AVCaptureDeviceInput(device: camera)
            self.captureSession.addInput(deviceInput)
            
            if let videoConnection = self.videoOutput.connectionWithMediaType(AVMediaTypeVideo) {
                
                videoConnection.videoOrientation = AVCaptureVideoOrientation.Portrait
            }
            
            self.captureSession.startRunning()
            
        } catch (_) {
            
            fatalError("Camera failed, close app!")
        }
    }
    
    func stopCameraSession() {
        
        self.captureSession.stopRunning()
        
        for input in self.captureSession.inputs as! [AVCaptureInput] {
            
            self.captureSession.removeInput(input)
        }
    }
    
    func getCameraFieldOfView() -> CGFloat {
        
        guard let camera = self.backCamera else {
            
            return 0
        }
        
        return CGFloat(camera.activeFormat.videoFieldOfView)
    }
    
    // MARK: AVCaptureVideoDataOutputSampleBufferDelegate
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        
        self.delegate?.arCameraSession(self, didOutputSampleBuffer: sampleBuffer)
    }
    
}