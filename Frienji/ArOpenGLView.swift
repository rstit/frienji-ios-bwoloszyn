//
//  ArOpenGLView.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import OpenGLES
import AVFoundation
import CoreLocation
import CoreMotion

class ArOpenGLView : UIView, ArCameraSessionProtocol {
    
    private static let kMinimumObjectDistance: CGFloat = 10
    static let kMaximumObjectDistance: CGFloat = 100
    
    private static let kMinimumBubbleSizeMultiplier: CGFloat = 0.2
    private static let kMaximumBubbleSizeMultiplier: CGFloat = 0.6
    
    // MARK: Properties
    
    private (set) var renderingActive:Bool = false
    
    private var openGLLayer: CAEAGLLayer!
    private var openGLContext: CVEAGLContext!
    
    private var colorRenderBuffer: GLuint = GLuint()
    
    private var viewportSize: CGSize = CGSizeZero
    var cameraAngle:CGFloat = 0
    
    //objects to be drawn
    private var cameraObject:CameraObject!
    private var cameraPositionSlot: GLuint!
    private var cameraTextureCoordinateSlot: GLuint!
    private var cameraTextureUniform: GLuint!
    private var cameraProgramHandle: GLuint!
    
    private var bubbleObjects = [Int64 : BubbleObject]()
    private var bubblePositionSlot: GLuint!
    private var bubbleTextureCoordinateSlot: GLuint!
    private var bubbleTextureUniform: GLuint!
    private var bubbleLogoTextureUniform: GLuint!
    private var bubbleProgramHandle: GLuint!
    
    //other data
    private var userLocation:CLLocation?
    private var userHeading:CLHeading?
    private var userInclination:CGFloat = 0
    
    // MARK: Life cycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    
        setUpView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpView()
    }
    
    // MARK: Set up
    
    private func setUpView() {
        
        self.openGLLayer = self.layer as! CAEAGLLayer
        self.openGLLayer.opaque = false
        
        configureContext()
        configureRenderBuffer()
        configureFrameBuffer()
        
        compileAndLinkShaders()
        
        setupDisplayLink()
        
        setUpCameraObject()
        
        self.contentScaleFactor = UIScreen.mainScreen().scale
    }
    
    // MARK: Layer
    
    override class func layerClass() -> AnyClass {
        
        // In order for our view to display OpenGL content, we need to set it's
        // default layer to be a CAEAGLayer
        return CAEAGLLayer.self
    }
    
    // MARK: State
    
    func activateRenderer() {
        
        self.renderingActive = true
    }
    
    func deactivateRenderer() {
        
        self.renderingActive = false
    }
    
    // MARK: Camera session
    
    func updateCameraTextureUsingSampleBuffer(sampleBuffer: CMSampleBuffer!) {
        
        self.cameraObject.updateCameraTextureUsingSampleBuffer(sampleBuffer)
    }
    
    // MARK: Context
    
    private func configureContext() {
        
        //create context
        self.openGLContext = EAGLContext(API: EAGLRenderingAPI.OpenGLES2)
        
        EAGLContext.setCurrentContext(self.openGLContext)
        
        glBlendEquation(UInt32(GL_FUNC_ADD))
        glBlendFunc(UInt32(GL_SRC_ALPHA), UInt32(GL_ONE_MINUS_SRC_ALPHA))
    }
    
    // MARK: Buffers
    
    private func configureRenderBuffer() {
        
        // A render buffer is an OpenGL objec that stores the rendered image to present to the screen.
        //   OpenGL will create a unique identifier for a render buffer and store it in a GLuint.
        //   So we call the glGenRenderbuffers function and pass it a reference to our colorRenderBuffer.
        glGenRenderbuffers(1, &self.colorRenderBuffer)

        // Then we tell OpenGL that whenever we refer to GL_RENDERBUFFER, it should treat that as our colorRenderBuffer.
        glBindRenderbuffer(UInt32(GL_RENDERBUFFER), self.colorRenderBuffer)
        
        // Finally, we tell our context that the render buffer for our layer is our colorRenderBuffer.
        self.openGLContext.renderbufferStorage(Int(GL_RENDERBUFFER), fromDrawable: self.openGLLayer)
    }
    
    private func configureFrameBuffer() {
        
        // A frame buffer is an OpenGL object for storage of a render buffer... amongst other things (tm).
        //   OpenGL will create a unique identifier for a frame vuffer and store it in a GLuint. So we
        //   make a GLuint and pass it to the glGenFramebuffers function to keep this identifier.
        var frameBuffer: GLuint = GLuint()
        glGenFramebuffers(1, &frameBuffer)
        
        // Then we tell OpenGL that whenever we refer to GL_FRAMEBUFFER, it should treat that as our frameBuffer.
        glBindFramebuffer(UInt32(GL_FRAMEBUFFER), frameBuffer)
        
        // Finally we tell the frame buffer that it's GL_COLOR_ATTACHMENT0 is our colorRenderBuffer. Oh.
        glFramebufferRenderbuffer(UInt32(GL_FRAMEBUFFER), UInt32(GL_COLOR_ATTACHMENT0), UInt32(GL_RENDERBUFFER), self.colorRenderBuffer)
    }
 
    // MARK: Shaders & Program
    
    private func compileAndLinkShaders() {
        
        // Compile our vertex and fragment shaders.
        compileAndLinkCameraShaders()
        compileAndLinkBubbleShaders()
    }
    
    private func compileAndLinkCameraShaders() {
        
        if let vertexShader = ShaderUtils.compileShaderWithName("CameraVertexShader", shaderType: UInt32(GL_VERTEX_SHADER), bundle: NSBundle(forClass: ArOpenGLView.self)),
            let fragmentShader = ShaderUtils.compileShaderWithName("CameraFragmentShader", shaderType: UInt32(GL_FRAGMENT_SHADER), bundle: NSBundle(forClass: ArOpenGLView.self)) {
            
            // Call glCreateProgram, glAttachShader, and glLinkProgram to link the vertex and fragment shaders into a complete program.
            self.cameraProgramHandle = glCreateProgram()
            glAttachShader(self.cameraProgramHandle, vertexShader)
            glAttachShader(self.cameraProgramHandle, fragmentShader)
            glLinkProgram(self.cameraProgramHandle)
            
            // Check for any errors.
            var linkSuccess: GLint = GLint()
            glGetProgramiv(self.cameraProgramHandle, UInt32(GL_LINK_STATUS), &linkSuccess)
            if (linkSuccess == GL_FALSE) {
                
                fatalError("Program link failed!")
            }
            
            // Finally, call glGetAttribLocation to get a pointer to the input values for the vertex shader, so we
            //  can set them in code. Also call glEnableVertexAttribArray to enable use of these arrays (they are disabled by default).
            self.cameraPositionSlot = UInt32(glGetAttribLocation(self.cameraProgramHandle, "Position"))
            glEnableVertexAttribArray(self.cameraPositionSlot)
            
            self.cameraTextureCoordinateSlot = UInt32(glGetAttribLocation(self.cameraProgramHandle, "TexCoordIn"))
            glEnableVertexAttribArray(self.cameraTextureCoordinateSlot)
            
            self.cameraTextureUniform = UInt32(glGetUniformLocation(self.cameraProgramHandle, "Texture"))
            
        } else {
            
            fatalError("Unable to compile shaders!")
        }
    }
    
    private func compileAndLinkBubbleShaders() {
        
        if let vertexShader = ShaderUtils.compileShaderWithName("BubbleVertexShader", shaderType: UInt32(GL_VERTEX_SHADER), bundle: NSBundle(forClass: ArOpenGLView.self)),
            let fragmentShader = ShaderUtils.compileShaderWithName("BubbleFragmentShader", shaderType: UInt32(GL_FRAGMENT_SHADER), bundle: NSBundle(forClass: ArOpenGLView.self)) {
            
            // Call glCreateProgram, glAttachShader, and glLinkProgram to link the vertex and fragment shaders into a complete program.
            self.bubbleProgramHandle = glCreateProgram()
            glAttachShader(self.bubbleProgramHandle, vertexShader)
            glAttachShader(self.bubbleProgramHandle, fragmentShader)
            glLinkProgram(self.bubbleProgramHandle)
            
            // Check for any errors.
            var linkSuccess: GLint = GLint()
            glGetProgramiv(self.bubbleProgramHandle, UInt32(GL_LINK_STATUS), &linkSuccess)
            if (linkSuccess == GL_FALSE) {
                
                fatalError("Program link failed!")
            }

            // Finally, call glGetAttribLocation to get a pointer to the input values for the vertex shader, so we
            //  can set them in code. Also call glEnableVertexAttribArray to enable use of these arrays (they are disabled by default).
            self.bubblePositionSlot = UInt32(glGetAttribLocation(self.bubbleProgramHandle, "Position"))
            glEnableVertexAttribArray(self.bubblePositionSlot)
            
            self.bubbleTextureCoordinateSlot = UInt32(glGetAttribLocation(self.bubbleProgramHandle, "TexCoordIn"))
            glEnableVertexAttribArray(self.bubbleTextureCoordinateSlot)
            
            self.bubbleTextureUniform = UInt32(glGetUniformLocation(self.bubbleProgramHandle, "Texture"))
            self.bubbleLogoTextureUniform = UInt32(glGetUniformLocation(self.bubbleProgramHandle, "LogoTexture"))
            
        } else {
            
            fatalError("Unable to compile shaders!")
        }
    }
    
    // MARK: Display link
    
    private func setupDisplayLink() {
        
        let displayLink = CADisplayLink(target: self, selector: #selector(ArOpenGLView.render(_:)))
        displayLink.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
    }
    
    // MARK: Render
    
    func render(displayLink: CADisplayLink) {
        
        if self.renderingActive {
            
            //set viewport
            let ratio = self.frame.size.width / self.frame.size.height
            glViewport(0, 0, GLint(self.frame.size.width * UIScreen.mainScreen().scale), GLint(self.frame.size.height * UIScreen.mainScreen().scale * ratio))
            self.viewportSize = CGSizeMake(self.frame.size.width * UIScreen.mainScreen().scale, self.frame.size.height * UIScreen.mainScreen().scale * ratio)
            
            glMatrixMode(UInt32(GL_PROJECTION))
            glLoadIdentity()
            glOrthof(Float(-self.viewportSize.width/2), Float(self.viewportSize.width/2), Float(-self.viewportSize.height/2), Float(self.viewportSize.height/2), -1, 1)
            
            //clear everything
            glClearColor(1.0, 1.0, 1.0, 1.0)
            glClear(UInt32(GL_COLOR_BUFFER_BIT))
            
            //draw camera frames
            glUseProgram(self.cameraProgramHandle)
            self.cameraObject.render(self.cameraPositionSlot, textureCoordinateSlot: self.cameraTextureCoordinateSlot, textureUniform: self.cameraTextureUniform)
            
            //draw bubbles
            glUseProgram(self.bubbleProgramHandle)
            
            glEnable(UInt32(GL_BLEND))
            
            for (_, bubble) in self.bubbleObjects {
            
                bubble.render(self.bubblePositionSlot, textureCoordinateSlot: self.bubbleTextureCoordinateSlot, textureUniform: self.bubbleTextureUniform, textureLogoUniform: self.bubbleLogoTextureUniform)
            }
            
            glDisable(UInt32(GL_BLEND))
            
            //show buffer content
            self.openGLContext.presentRenderbuffer(Int(GL_RENDERBUFFER))
        }
    }
    
    // MARK: Camera object
    
    private func setUpCameraObject() {
        
        self.cameraObject = CameraObject(context: self.openGLContext)
    }
    
    // MARK: Bubbles objects
    
    private func updateBubbleObjectsForTraces(traces:[Trace]) {
        
        let tracesIds = traces.map { (trace:Trace) -> Int64 in
            
            return trace.dbID
        }
        
        let currentBubbles = self.bubbleObjects
        
        //remove old ones
        for (traceID, _) in currentBubbles {
        
            if !tracesIds.contains(traceID) {
                
                //not in new traces to show - remove
                self.bubbleObjects[traceID] = nil
            }
        }
        
        //add new ones
        for trace in traces {
            
            //check if this trace have already bubble
            if self.self.bubbleObjects[trace.dbID] == nil {
                
                self.self.bubbleObjects[trace.dbID] = BubbleObject(trace: trace)
            }
        }
    }
    
    private func calculateBubbleSizes() {
        
        guard let userLocation = self.userLocation else {
            
            return
        }
        
        for (_, bubble) in self.bubbleObjects {
            
            let bubbleLocation = bubble.trace.location
            let bubbleDistance = CGFloat(userLocation.distanceFromLocation(bubbleLocation))
            
            let normalizedDistance = min(ArOpenGLView.kMaximumObjectDistance, max(ArOpenGLView.kMinimumObjectDistance, bubbleDistance))
            let sizeRatio = (normalizedDistance - ArOpenGLView.kMinimumObjectDistance) / (ArOpenGLView.kMaximumObjectDistance - ArOpenGLView.kMinimumObjectDistance)
            
            let bubbleSizeMultiplier = sizeRatio * (ArOpenGLView.kMaximumBubbleSizeMultiplier - ArOpenGLView.kMinimumBubbleSizeMultiplier) + ArOpenGLView.kMinimumBubbleSizeMultiplier
            let bubbleSize = bubbleSizeMultiplier * self.frame.size.width
            
            bubble.calculateBubbleSizeForDesiredSize(bubbleSize, viewportWidth: self.frame.size.width, viewportHeight: self.frame.size.height, animated: true)
        }
    }
    
    private func calculateBubblePositions() {
        
        guard let userLocation = self.userLocation, let userHeading = self.userHeading else {
            
            return
        }
        
        var minFieldOfViewAngle = CGFloat(userHeading.trueHeading) - self.cameraAngle/2
        var maxFieldOfViewAngle = CGFloat(userHeading.trueHeading) + self.cameraAngle/2
        
        if minFieldOfViewAngle < 0 {
            
            minFieldOfViewAngle = 360 - abs(minFieldOfViewAngle)
        }
        
        if maxFieldOfViewAngle > 360 {
            
            maxFieldOfViewAngle = maxFieldOfViewAngle - 360
        }
        
        for (_, bubble) in self.bubbleObjects {

            let bubbleLocation = bubble.trace.location
            let bearing = LocationManager.sharedInstance.getBearingBetweenTwoPoints(userLocation, point2: bubbleLocation)
           
            bubble.calculateBubblePositionForBearing(bearing, minFieldOfViewAngle: minFieldOfViewAngle, maxFieldOfViewAngle: maxFieldOfViewAngle)
        }
    }
    
    private func calculateBubbleYPositions() {
        
        for (_, bubble) in self.bubbleObjects {
           
            bubble.calculateBubblePositionForYMotion(self.userInclination)
        }
    }
    
    // MARK: ArCameraSessionProtocol
    
    func arCameraSession(session: ArCameraSession, didOutputSampleBuffer sampleBuffer: CMSampleBuffer) {
        
        self.updateCameraTextureUsingSampleBuffer(sampleBuffer)
    }
    
    // MARK: Data
    
    func updateWithTraces(traces:[Trace]) {
        
        self.updateBubbleObjectsForTraces(traces)
        
        self.calculateBubbleSizes()
        self.calculateBubblePositions()
        self.calculateBubbleYPositions()
    }
    
    func updateUserLocation(location:CLLocation?) {
        
        self.userLocation = location
        
        self.calculateBubbleSizes()
    }
    
    func updateHeading(heading:CLHeading?) {
        
        self.userHeading = heading
        
        self.calculateBubblePositions()
    }
    
    func updateInclination(inclination:CGFloat) {
        
        self.userInclination = inclination
        
        self.calculateBubbleYPositions()
    }
    
    // MARK: Touch
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
        if let touchLocation = touches.first?.locationInView(self) {
            
            for (_, bubble) in self.bubbleObjects {
                
                let boundingBox = bubble.getBoundingBoxForScreenPixelCoordinates(self.frame.size, viewportSize: self.viewportSize)
                print("box: \(boundingBox), bearing: \(bubble.bubbleBearing), userHeading: \(self.userHeading?.trueHeading)")
                
                if boundingBox.contains(touchLocation) {
                    
                    //bubble touched
                    print("bubble touched!")

                    //to do
                    
                    break
                }
            }
        }
    }
    
}