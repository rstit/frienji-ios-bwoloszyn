//
//  ArViewController.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift
import RxCocoa
import CoreLocation
import CoreMotion

class ArViewController: UIViewController, StoryboardLoad {

    static var storyboardId: String = "Ar"
    
    @IBOutlet weak var arOpenGLView: ArOpenGLView!
    @IBOutlet weak var profileButton: UIButton!
    
    //properties
    private var viewModel:ArViewModel!
    private var cameraSession:ArCameraSession!
    
    private let disposeBag = DisposeBag()
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewModel = ArViewModel()
        
        registerForAppLifecycleEvents()
        
        setUpBindings()
        
        self.cameraSession = ArCameraSession()
        self.cameraSession.delegate = self.arOpenGLView
        self.arOpenGLView.cameraAngle = self.cameraSession.getCameraFieldOfView()
        
        self.navigationController?.navigationBarHidden = true;
    }
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        activateRenderer()
        self.navigationController?.navigationBarHidden = true;
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        deactivateRenderer()
    }
    
    // MARK: Actions
    
    @IBAction func onProfileButtonTouch(sender: AnyObject) {
        
        //to do
    }
    
    // MARK: Notifications
    
    private func registerForAppLifecycleEvents() {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ArViewController.appWillEnterForeground), name: UIApplicationWillEnterForegroundNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ArViewController.appWillEnterBackground), name: UIApplicationWillResignActiveNotification, object: nil)
    }
    
    func appWillEnterForeground() {
        
        activateRenderer()
    }
    
    func appWillEnterBackground() {
        
        deactivateRenderer()
    }
    
    // MARK: Bindings
    
    private func setUpBindings() {
        
        LocationManager.sharedInstance.currentLocation.asObservable().bindNext { [unowned self] (userLocation:CLLocation?) in
            
            self.arOpenGLView.updateUserLocation(userLocation)
            
            if let location = userLocation {
                
                self.viewModel.fetchTracesAroundUserLocation(location.coordinate)
            }
            
        }.addDisposableTo(self.disposeBag)
        
        LocationManager.sharedInstance.currentHeading.asObservable().bindNext { [unowned self] (heading:CLHeading?) in
            
            self.arOpenGLView.updateHeading(heading)
            
        }.addDisposableTo(self.disposeBag)
        
        MotionManager.sharedInstance.currentInclination.asObservable().bindNext { [unowned self] (inclination:CGFloat) in
            
            self.arOpenGLView.updateInclination(inclination)
            
        }.addDisposableTo(self.disposeBag)
        
        //traces
        self.viewModel.traces.asObservable().bindNext { [unowned self] (traces:[Trace]) in
            
            self.arOpenGLView.updateWithTraces(traces)
            
        }.addDisposableTo(self.disposeBag)
    }
    
    // MARK: Render
    
    func activateRenderer() {
        
        self.cameraSession.startCameraSession()
        self.arOpenGLView.activateRenderer()
        self.arOpenGLView.updateUserLocation(LocationManager.sharedInstance.currentLocation.value)
        
        LocationManager.sharedInstance.startLocationUpdates()
        MotionManager.sharedInstance.startMotionUpdates()
    }
    
    func deactivateRenderer() {
        
        self.cameraSession.stopCameraSession()
        self.arOpenGLView.deactivateRenderer()
        
        LocationManager.sharedInstance.stopLocationUpdates()
        MotionManager.sharedInstance.stopMotionUpdates()
    }

}

