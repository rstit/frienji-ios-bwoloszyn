//
//  ArViewModel.swift
//  Traces
//
//  Created by Adam Szeremeta on 15.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation
import Parse

class ArViewModel {
    
    let traces = Variable([Trace]())
    
    // MARK: Init
    
    init() {
        
    }
    
    // MARK: Api
    
    func fetchTracesAroundUserLocation(location:CLLocationCoordinate2D) {
        
//        let coordinates  = ["latitude" : location.latitude, "longitude" : location.longitude];
//
//        PFCloud.callFunctionInBackground("getNearestUsersAndThreads", withParameters: coordinates) { (objects, error) -> () in
//            if objects == nil || error != nil
//            {
//                print("---ERROR!");
//                return;
//            }
//            
////            let frienjis = NSMutableArray();
////            for user : PFUser in objects as! NSArray
////            {
////                frienjis.addObject(user);
////            }
//        }
        
            
//            // Blocked users?
//            // --------------
//            
//            NSArray *idBlockedUsers = [UserManager sharedInstance].currentUser.getBlockedUserIds;
//            
//            // Treating
//            // --------
//            
//            NSMutableArray *users = @[].mutableCopy;
//            for(PFUser *object in objects)
//            {
//                // User
//                // ----
//                
//                User *user = [[User alloc] initWithNetworkObject:object[@"user"]];
//                if(!user)
//                continue;
//                
//                // Discard blocked users
//                // ---------------------
//                
//                if([idBlockedUsers containsObject:user.objectId])
//                continue;
//                
//                // Keep the user
//                // -------------
//                
//                [users addObject:user];
//            }
//            
//            // Update the global list of user
//            // ------------------------------
//            
//            [UserManager sharedInstance].usersAroundMe = users;
//            
//            // Completion
//            // ----------
//            
//            completion(users);
//        }];
        
        //to do
        
        let trace = Trace.insertNewObjectForEntity(inContext: DatabaseHelper.sharedInstance.mainManagedObjectContext)
        trace.dbID = 1
        trace.latitude = 55
        trace.longitude = 25
        trace.hasPrize = true
        trace.logoImage = "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTdgWgt-Kn--IA9xbmZbyszqWQb-awLW93At9eJaC6z7U431DIi"
        
        self.traces.value = [trace]
    }
}