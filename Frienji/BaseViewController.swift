//
//  BaseViewController.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController : UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //register for keyboard notifications
        registerForKeyboardNotifications()
        
        //add tap gesture to view to dismiss keyboard
        addTapGestureToView()
    }
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    // MARK: Keyboard Notification
    
    private func registerForKeyboardNotifications() {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BaseViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BaseViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // MARK: Keyboard Handling

    func keyboardWillShow(notification: NSNotification) {
        
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let animationTime = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
        
        keyboardWillShowWithSize(keyboardSize, animationTime: animationTime ?? 0.33)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        keyboardWillHide()
    }
    
    // MARK: Keyboard Handling
    
    func keyboardWillShowWithSize(keyboardSize: CGSize?, animationTime:Double) {
        
        //implement in controller
    }
    
    func keyboardWillHide() {
        
        //implement in controller
    }

    // MARK: Tap Gesture
    
    private func addTapGestureToView() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delegate = self
        
        self.view.addGestureRecognizer(tapGesture)
    }
    
    // MARK: UIGestureRecognizerDelegate
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        
        return !(touch.view!.isKindOfClass(UIButton.self) || touch.view!.isKindOfClass(UITextField.self))
    }
    
    // MARK: Keyboard dismiss
    
    func dismissKeyboard() {
        
        self.view.endEditing(true)
    }

}