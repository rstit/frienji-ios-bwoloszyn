precision mediump float;

uniform sampler2D Texture;
varying vec2 CameraTextureCoord;

uniform sampler2D LogoTexture;

#define RADIUS 0.5
#define CENTER vec2(0.5, 0.5)

#define REFRACTIVE_INDEX 0.71

#define AMBIENT_COLOUR vec3(0.5, 0.5, 0.5)

#define SPECULAR_COLOUR vec3(0.5, 0.5, 0.5)
#define SPECULAR_INTENSITY 6.0

void main() {
    
    highp vec2 textCoord = vec2(1.0 - CameraTextureCoord.x, 1.0 - CameraTextureCoord.y);
    
    // Discard pixels to make sphere
    highp float distance = distance(CENTER, textCoord.xy);
    if (step(distance, RADIUS) <= 0.0)
        discard;
    
    highp float distanceFromCenter = distance / RADIUS;
    
    // glass without aberation
    highp float normalizedDepth = RADIUS * sqrt(1.0 - distanceFromCenter * distanceFromCenter);
    highp vec3 sphereNormal = normalize(vec3(textCoord.xy - CENTER, normalizedDepth));
    highp vec3 refractedVector = refract(vec3(0.0, 0.0, -1.0), sphereNormal, REFRACTIVE_INDEX);
    
    highp vec2 finalSphereCoordinate = (refractedVector.xy + 1.0) * 0.5;
    highp vec3 finalSphereColor = texture2D(Texture, finalSphereCoordinate).rgb;
    
    // logo
    highp float imageRadius = RADIUS * 0.85;
    highp float imageDistanceFromCenter = distance / imageRadius;
    highp float imageNormalizedDepth = imageRadius * sqrt(1.0 - imageDistanceFromCenter * imageDistanceFromCenter);
    highp vec3 imageNormal = normalize(vec3(CameraTextureCoord.xy - CENTER, imageNormalizedDepth));
    highp vec3 imageRefractedVector = refract(vec3(0.0, 0.0, -1.0), imageNormal, 0.5);
    imageRefractedVector.xy = -imageRefractedVector.xy;
    highp vec2 finalImageCoordinate = (imageRefractedVector.xy + 1.0) * 0.5;
    
    highp vec4 logoColor = texture2D(LogoTexture, finalImageCoordinate);
    
    if (logoColor.a > 0.0) {
       
        highp float value = 1.0 - smoothstep(0.85, 1.0, imageDistanceFromCenter); //smooth transition between logo and bubble on edges
        finalSphereColor = mix(finalSphereColor, logoColor.rgb, value * 0.85);
    }
    
    // ambient light
    highp float lightingIntensity = 0.3 * (1.0 - pow(clamp(dot(vec3(0.0, 0.0, 1.0), sphereNormal), 0.0, 1.0), 0.25));
    finalSphereColor += lightingIntensity * AMBIENT_COLOUR;
    
    // specular light
    lightingIntensity = clamp(dot(normalize(vec3(-0.5, -0.5, 1.0)), sphereNormal), 0.0, 1.0);
    lightingIntensity = pow(lightingIntensity, SPECULAR_INTENSITY);
    finalSphereColor += (SPECULAR_COLOUR * lightingIntensity) * 0.3;
    
    // set final pixel color
    gl_FragColor = vec4(finalSphereColor, 0.9);
}
