//
//  BubbleObject.swift
//  Traces
//
//  Created by Adam Szeremeta on 14.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import OpenGLES
import AVFoundation
import RxSwift
import RxCocoa
import SDWebImage

class BubbleObject {
    
    static let kCoordinateSpaceSize:CGFloat = 2 //from -1 to 1
    static let kLowPassFilterValue:CGFloat = 0.15
    static let kSizeAnimationDelta:Double = 0.03
    
    private (set) var bubbleSize: CGSize = CGSizeZero
    private (set) var bubblePosition: CGPoint = CGPointZero
    private (set) var bubbleBearing: CGFloat = 0
    
    private var logoTextureData:[GLubyte]?
    private var logoTextureWidth:Int32?
    private var logoTextureHeight:Int32?
    private var logoTexturePointer:GLuint?
    
    private var currentSizeAnimationDelta:Double = 0
    private var sizeDisposeBag:DisposeBag? = DisposeBag()
    
    // MARK: Bubble texture data
    
    private var bubbleVertices: (Vertex, Vertex, Vertex, Vertex) = (
        
        Vertex(position: (0, 0, 0), textureCoordinate: (1, 1)),
        Vertex(position: (0, 0, 0), textureCoordinate: (1, 0)),
        Vertex(position: (0, 0, 0), textureCoordinate: (0, 0)),
        Vertex(position: (0, 0, 0), textureCoordinate: (0, 1))
    )
    
    private var bubbleIndices: (GLubyte, GLubyte, GLubyte, GLubyte, GLubyte, GLubyte) = (
        
        0, 1, 2,
        2, 3, 0
    )
    
    // MARK: Properties
    
    private var shouldBubbleBeDrawn = false
    
    private var indexBuffer: GLuint = GLuint()
    private var vertexBuffer: GLuint = GLuint()
    
    private (set) var trace:Trace!
    
    // MARK: Life cycle
    
    init(trace:Trace) {
        
        self.trace = trace
        
        loadTraceLogoAsTexture()
        setupVBOs()
    }

    // MARK: Setup
    
    private func setupVBOs() {
        
        // Setup VertexColor Buffer Objects
        
        glGenBuffers(1, &self.vertexBuffer)
        glGenBuffers(1, &self.indexBuffer)
    }
    
    // MARK: Texture
    
    private func loadTraceLogoAsTexture() {
        
        guard let logo = self.trace.logoImage, let logoUrl = NSURL(string: logo) else {
            
            return
        }
        
        //download logo
        let downloader = SDWebImageDownloader.sharedDownloader()
        downloader.downloadImageWithURL(logoUrl, options: SDWebImageDownloaderOptions.HighPriority, progress: { (downloaded:Int, left:Int) in
            
        }) { (image:UIImage!, data:NSData!, error:NSError!, result:Bool) in
            
            if image != nil {
                
                self.createLogoTextureDataFromImage(image)
            }
        }
    }
    
    private func createLogoTextureDataFromImage(logoImage:UIImage) {
        
        guard let logoCGImage = logoImage.CGImage else {
            
            return
        }
        
        let logoWidth = CGImageGetWidth(logoCGImage)
        let logoHeight = CGImageGetHeight(logoCGImage)
        
        var spriteData: [GLubyte] = Array(count: Int(logoWidth * logoHeight * 4), repeatedValue: 0)
        let bitmapInfo = CGImageAlphaInfo.PremultipliedLast.rawValue
        
        if let spriteContext = CGBitmapContextCreate(&spriteData, logoWidth, logoHeight, 8, logoWidth * 4, CGImageGetColorSpace(logoCGImage), bitmapInfo) {
            
            CGContextDrawImage(spriteContext, CGRectMake(0, 0, CGFloat(logoWidth) , CGFloat(logoHeight)), logoCGImage)

            self.logoTextureData = spriteData
            self.logoTextureWidth = Int32(logoWidth)
            self.logoTextureHeight = Int32(logoHeight)
        }
    }
    
    private func createLogoTexture(textureLogoUniform:GLuint) {
        
        //check if we have logo texture
        if let textureData = self.logoTextureData, let textureWidth = self.logoTextureWidth, let textureHeight = self.logoTextureHeight {
         
            glActiveTexture(UInt32(GL_TEXTURE1))
            
            self.logoTexturePointer = GLuint()
            glGenTextures(1, &self.logoTexturePointer!)
            glBindTexture(UInt32(GL_TEXTURE_2D), self.logoTexturePointer!)
            
            glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_MIN_FILTER), GL_NEAREST)
            
            glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_WRAP_S), Int32(GL_CLAMP_TO_EDGE))
            glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_WRAP_T), Int32(GL_CLAMP_TO_EDGE))
            
            glTexImage2D(UInt32(GL_TEXTURE_2D), 0, GL_RGBA, Int32(textureWidth), Int32(textureHeight), 0, UInt32(GL_RGBA), UInt32(GL_UNSIGNED_BYTE), textureData)

            glUniform1i(Int32(textureLogoUniform), 1)
        }
    }
    
    private func destroyLogoTexture() {
        
        if var pointer = self.logoTexturePointer {
            
            glDeleteTextures(1, &pointer)
        }
    }
    
    // MARK: Animations
    
    private func animateBubbleSize(newSize:CGFloat, viewportWidth:CGFloat, viewportHeight:CGFloat) {
        
        //calculate size
        let newWidth = newSize * BubbleObject.kCoordinateSpaceSize / viewportWidth
        let newHeight = newSize * BubbleObject.kCoordinateSpaceSize / viewportHeight
        
        //do smooth transition between old and new size
        self.currentSizeAnimationDelta = 0
        self.sizeDisposeBag = DisposeBag()
        
        Observable<Int>.interval(BubbleObject.kSizeAnimationDelta, scheduler: MainScheduler.instance).bindNext({ [unowned self] (delta:Int) in
            
            self.currentSizeAnimationDelta += BubbleObject.kSizeAnimationDelta
            
            if self.currentSizeAnimationDelta < 1 {
                
                let finalWidth = self.bubbleSize.width + CGFloat(BubbleObject.kSizeAnimationDelta) * (newWidth - self.bubbleSize.width)
                let finalHeight = self.bubbleSize.height + CGFloat(BubbleObject.kSizeAnimationDelta) * (newHeight - self.bubbleSize.height)
                
                self.bubbleSize = CGSizeMake(finalWidth, finalHeight)
                self.updateBubbleVertices()
                
            } else {
                
                self.sizeDisposeBag = nil
            }
            
        }).addDisposableTo(self.sizeDisposeBag!)
    }
    
    // MARK: Data
    
    func getBoundingBoxForScreenPixelCoordinates(screenSize:CGSize, viewportSize:CGSize) -> CGRect {
        
        let ratio = screenSize.width / viewportSize.width
        let heightDiff = abs(screenSize.height - viewportSize.height) / 2
        
        let width = screenSize.width * self.bubbleSize.width * ratio
        let height = screenSize.height * self.bubbleSize.height * ratio
                
        let originX = screenSize.width * (1 + self.bubblePosition.x) / BubbleObject.kCoordinateSpaceSize
        let originY = screenSize.height - ((screenSize.height * (1 + self.bubblePosition.y) / BubbleObject.kCoordinateSpaceSize) + height) - heightDiff
                
        return CGRectMake(originX, originY, width, height)
    }
    
    func calculateBubbleSizeForDesiredSize(desiredSize:CGFloat, viewportWidth:CGFloat, viewportHeight:CGFloat, animated:Bool) {
        
        precondition(viewportWidth > 0)
        precondition(viewportHeight > 0)
    
        if animated {
            
            self.animateBubbleSize(desiredSize, viewportWidth: viewportWidth, viewportHeight: viewportHeight)
            
        } else {
            
            let newWidth = desiredSize * BubbleObject.kCoordinateSpaceSize / viewportWidth
            let newHeight = desiredSize * BubbleObject.kCoordinateSpaceSize / viewportHeight
            
            self.bubbleSize = CGSizeMake(newWidth, newHeight)
            self.updateBubbleVertices()
        }
    }
    
    func calculateBubblePositionForYMotion(inclination:CGFloat) {
        
        let kDefaultBubbleYOrigin = -self.bubbleSize.height/2
        let newYPosition = kDefaultBubbleYOrigin - inclination
        
        //low pass filter
        let yPosition = BubbleObject.kLowPassFilterValue * self.bubblePosition.y + (1.0 - BubbleObject.kLowPassFilterValue) * newYPosition
        self.bubblePosition = CGPointMake(self.bubblePosition.x, yPosition)
        
        updateBubbleVertices()
    }
    
    func calculateBubblePositionForBearing(bearing:CGFloat, minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat) {
        
        precondition(minFieldOfViewAngle > 0)
        precondition(maxFieldOfViewAngle > 0)
        
        //check if bubble is in field of view
        if self.isBearingInRangeOfFieldOfView(bearing, minFieldOfViewAngle: minFieldOfViewAngle, maxFieldOfViewAngle: maxFieldOfViewAngle) {
            
            let angleRange = maxFieldOfViewAngle - minFieldOfViewAngle
            let bubbleBearing = angleRange - (maxFieldOfViewAngle - bearing)
            
            let position = -1 + (bubbleBearing * BubbleObject.kCoordinateSpaceSize / angleRange) //this is center so we have to substract half of the width
            let newPosition = CGPointMake(position - self.bubbleSize.width/2, 0)
            
            //low pass filter
            let xPosition = BubbleObject.kLowPassFilterValue * self.bubblePosition.x + (1.0 - BubbleObject.kLowPassFilterValue) * newPosition.x
            self.bubblePosition = CGPointMake(xPosition, self.bubblePosition.y)
            
            self.bubbleBearing = bearing
            
            updateBubbleVertices()
            
            self.shouldBubbleBeDrawn = true
            return
            
        } else if minFieldOfViewAngle > maxFieldOfViewAngle {
            
            let minAngle = 360 - minFieldOfViewAngle
            let maxAngle = maxFieldOfViewAngle + minAngle
            let bubbleBearing = bearing + minAngle
            
            calculateBubblePositionForBearing(bubbleBearing, minFieldOfViewAngle: minAngle, maxFieldOfViewAngle: maxAngle)
            
            self.shouldBubbleBeDrawn = false
            return
        }
        
        self.shouldBubbleBeDrawn = false
    }
    
    private func isBearingInRangeOfFieldOfView(bearing:CGFloat, minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat) -> Bool {
        
        if minFieldOfViewAngle > maxFieldOfViewAngle {
            
            return false
        }

        let fieldOfViewMargin = (maxFieldOfViewAngle - minFieldOfViewAngle) / 2
        
        return minFieldOfViewAngle < maxFieldOfViewAngle
            && (bearing < maxFieldOfViewAngle + fieldOfViewMargin && bearing > minFieldOfViewAngle - fieldOfViewMargin)
    }

    private func updateBubbleVertices() {
        
        self.bubbleVertices.0.position = (CFloat(self.bubblePosition.x + self.bubbleSize.width), CFloat(self.bubblePosition.y), 0) //right bottom
        self.bubbleVertices.1.position = (CFloat(self.bubblePosition.x + self.bubbleSize.width), CFloat(self.bubblePosition.y + self.bubbleSize.height), 0) //right top
        self.bubbleVertices.2.position = (CFloat(self.bubblePosition.x), CFloat(self.bubblePosition.y + self.bubbleSize.height), 0) //left top
        self.bubbleVertices.3.position = (CFloat(self.bubblePosition.x), CFloat(self.bubblePosition.y), 0) //left bottom
    }
    
    // MARK: Render
    
    func render(positionSlot:GLuint, textureCoordinateSlot:GLuint, textureUniform:GLuint, textureLogoUniform:GLuint) {
        
        if self.shouldBubbleBeDrawn {
            
            //bind buffers
            glBindBuffer(UInt32(GL_ARRAY_BUFFER), self.vertexBuffer)
            glBufferData(UInt32(GL_ARRAY_BUFFER), Int(sizeofValue(self.bubbleVertices)), &self.bubbleVertices, UInt32(GL_STATIC_DRAW))
            
            glBindBuffer(UInt32(GL_ELEMENT_ARRAY_BUFFER), self.indexBuffer)
            glBufferData(UInt32(GL_ELEMENT_ARRAY_BUFFER), Int(sizeofValue(self.bubbleIndices)), &self.bubbleIndices, UInt32(GL_STATIC_DRAW))
            
            //bind logo texture
            self.createLogoTexture(textureLogoUniform)

            //set up pointers
            let positionSlotPointer = UnsafePointer<Int>(bitPattern: 0)
            glVertexAttribPointer(positionSlot, 3, UInt32(GL_FLOAT), UInt8(GL_FALSE), Int32(sizeof(Vertex)), positionSlotPointer)
            
            let textCoordinatePointer = UnsafePointer<Int>(bitPattern: sizeof(Float) * 3)
            glVertexAttribPointer(textureCoordinateSlot, 2, UInt32(GL_FLOAT), UInt8(GL_FALSE), Int32(sizeof(Vertex)), textCoordinatePointer)
            
            let vertextBufferOffset = UnsafePointer<Int>(bitPattern: 0)
            glDrawElements(UInt32(GL_TRIANGLES), Int32(GLfloat(sizeofValue(self.bubbleIndices)) / GLfloat(sizeofValue(self.bubbleIndices.0))), UInt32(GL_UNSIGNED_BYTE), vertextBufferOffset)
            
            //destroy logo texture
            self.destroyLogoTexture()
        }
    }
    
}