//
//  CameraObject.swift
//  Traces
//
//  Created by Adam Szeremeta on 14.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import OpenGLES
import AVFoundation

class CameraObject {
  
    // MARK: Camera texture data
    
    private var cameraVertices: (Vertex, Vertex, Vertex, Vertex) = (
        
        Vertex(position: (1, -1, 0), textureCoordinate: (1, 1)),
        Vertex(position: (1, 1, 0), textureCoordinate: (1, 0)),
        Vertex(position: (-1, 1, 0), textureCoordinate: (0, 0)),
        Vertex(position: (-1, -1, 0), textureCoordinate: (0, 1))
    )
    
    private var cameraIndices: (GLubyte, GLubyte, GLubyte, GLubyte, GLubyte, GLubyte) = (
        
        0, 1, 2,
        2, 3, 0
    )
    
    // MARK: Properties

    private var indexBuffer: GLuint = GLuint()
    private var vertexBuffer: GLuint = GLuint()
    
    private var videoTextureCache: CVOpenGLESTextureCache?
    private var videoTexture: CVOpenGLESTextureRef?
    private (set) var videoTextureID: GLuint?
    
    private var textureWidth:CGFloat = 0
    private var textureHeight:CGFloat = 0

    // MARK: Life cycle
    
    init(context:CVEAGLContext) {
        
        createTextureCache(context)
        
        setupVBOs()
    }
    
    // MARK: Setup
    
    private func createTextureCache(context:CVEAGLContext) {
        
        //create texture cache
        CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, nil, context, nil, &self.videoTextureCache)
    }
    
    private func setupVBOs() {
        
        // Setup Vertex Buffer Objects
        
        glGenBuffers(1, &self.vertexBuffer)
        glGenBuffers(1, &self.indexBuffer)
    }
    
    // MARK: Texture
    
    func updateCameraTextureUsingSampleBuffer(sampleBuffer: CMSampleBuffer!) {
        
        dispatch_async(dispatch_get_main_queue(), {
            
            self.videoTextureID = self.getTextureFromSampleBuffer(sampleBuffer)
        });
    }
    
    func getCameraTextureSize() -> CGSize {
        
        return CGSizeMake(self.textureWidth, self.textureHeight)
    }
    
    private func cleanUpVideoTexture() {
        
        if let textureCache = self.videoTextureCache {
            
            self.videoTexture = nil
            CVOpenGLESTextureCacheFlush(textureCache, 0)
        }
    }
    
    private func getTextureFromSampleBuffer(sampleBuffer: CMSampleBuffer!) -> GLuint? {
        
        self.cleanUpVideoTexture()
        
        if let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) {
            
            self.textureWidth = CGFloat(CVPixelBufferGetWidth(imageBuffer))
            self.textureHeight = CGFloat(CVPixelBufferGetHeight(imageBuffer))
            
            CVPixelBufferLockBaseAddress(imageBuffer, 0)
            
            CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault, self.videoTextureCache!, imageBuffer, nil, UInt32(GL_TEXTURE_2D), GL_RGBA, GLsizei(self.textureWidth), GLsizei(self.textureHeight), UInt32(GL_BGRA), UInt32(GL_UNSIGNED_BYTE), 0, &self.videoTexture)
            
            let textureID = CVOpenGLESTextureGetName(self.videoTexture!)
            glBindTexture(UInt32(GL_TEXTURE_2D), textureID)
            
            glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_WRAP_S), Int32(GL_CLAMP_TO_EDGE))
            glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_WRAP_T), Int32(GL_CLAMP_TO_EDGE))
            
            glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_MIN_FILTER), GL_LINEAR)
            glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_MAG_FILTER), GL_LINEAR)
            
            CVPixelBufferUnlockBaseAddress(imageBuffer, 0)
            
            return textureID
        }
        
        return nil
    }
    
    // MARK: Render
    
    func render(positionSlot:GLuint, textureCoordinateSlot:GLuint, textureUniform:GLuint) {
        
        //bind buffers
        glBindBuffer(UInt32(GL_ARRAY_BUFFER), self.vertexBuffer)
        glBufferData(UInt32(GL_ARRAY_BUFFER), Int(sizeofValue(self.cameraVertices)), &self.cameraVertices, UInt32(GL_STATIC_DRAW))
        
        glBindBuffer(UInt32(GL_ELEMENT_ARRAY_BUFFER), self.indexBuffer)
        glBufferData(UInt32(GL_ELEMENT_ARRAY_BUFFER), Int(sizeofValue(self.cameraIndices)), &self.cameraIndices, UInt32(GL_STATIC_DRAW))
        
        //set up pointers
        let positionSlotPointer = UnsafePointer<Int>(bitPattern: 0)
        glVertexAttribPointer(positionSlot, 3, UInt32(GL_FLOAT), UInt8(GL_FALSE), Int32(sizeof(Vertex)), positionSlotPointer)
        
        let textCoordinatePointer = UnsafePointer<Int>(bitPattern: sizeof(Float) * 3)
        glVertexAttribPointer(textureCoordinateSlot, 2, UInt32(GL_FLOAT), UInt8(GL_FALSE), Int32(sizeof(Vertex)), textCoordinatePointer)
        
        glActiveTexture(UInt32(GL_TEXTURE0))
        if let textureId = self.videoTextureID {
            
            glBindTexture(UInt32(GL_TEXTURE_2D), textureId)
            glUniform1i(Int32(textureUniform), 0)
        }
        
        let vertextBufferOffset = UnsafePointer<Int>(bitPattern: 0)
        glDrawElements(UInt32(GL_TRIANGLES), Int32(GLfloat(sizeofValue(self.cameraIndices)) / GLfloat(sizeofValue(self.cameraIndices.0))), UInt32(GL_UNSIGNED_BYTE), vertextBufferOffset)
    }

}