//
//  ConfigurationsHelper.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

class ConfigurationsHelper {
    
    private enum Configuration : String {
        
        case Debug = "Debug"
        case Staging = "Staging"
        case Production = "Production"
    }
    
    private enum EnvironmentProperty : String {
        
        case BackendUrl = "backendUrl"
        case GoogleApiKey = "googleApiKey"
    }
    
    private let kConfigurationKey = "Configuration"
    private let kEnvironmentPlistName = "EnvironmentVariables"
    
    private var activeConfiguration:Configuration?
    private var activeEnviromentDictionary:NSDictionary!
    
    // MARK: Shared instance
    
    static let sharedInstance = ConfigurationsHelper()
    
    // MARK: Init
    
    private init() {
        
        let bundle = NSBundle(forClass: ConfigurationsHelper.self)
        
        //load configuration name
        let configurationName = bundle.infoDictionary![kConfigurationKey] as! String
        self.activeConfiguration = Configuration(rawValue: configurationName)
        
        //load our configuration plist
        let environmentsPath = bundle.pathForResource(kEnvironmentPlistName, ofType: "plist")
        let environmentsDict = NSDictionary(contentsOfFile: environmentsPath!)
        
        self.activeEnviromentDictionary = environmentsDict![self.activeConfiguration!.rawValue] as! NSDictionary
    }
    
    // MARK: Runtime
    
    func isRunningInRelease() -> Bool {
        
        guard let activeConfig = self.activeConfiguration else {
            
            return false
        }
        
        return activeConfig == Configuration.Staging || activeConfig == Configuration.Production
    }
    
    // MARK: Environment properties
    
    func getBackendUrl() -> String {
        
        return self.activeEnviromentDictionary[EnvironmentProperty.BackendUrl.rawValue] as! String
    }
    
    func getGoogleApiKey() -> String {
        
        return self.activeEnviromentDictionary[EnvironmentProperty.GoogleApiKey.rawValue] as! String
    }
    
}