//
//  DatabaseEntity.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreData

protocol DatabaseEntity: class {
    
    static var entityName: String {get}
    
}