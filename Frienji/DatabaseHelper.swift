//
//  DatabaseHelper.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

class DatabaseHelper : DatabaseManager {
    
    // MARK: Shared Instance
    
    static let sharedInstance = DatabaseHelper()
    
    // MARK: Life cycle
    
    override init() {
        super.init()
        
        let applicationStore = PersistentStoreCoreDataItem(name: "Traces")
        self.setPersistentStoreItems([applicationStore])
    }
    
}
