//
//  DatabaseManager.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreData

/**
 * Class for data storage using in memory CoreData persistent store.
 */
class DatabaseManager : NSObject {
    
    // MARK: Properties
    
    private var persistentStoreItems: [PersistentStoreCoreDataItem]?
    
    // Persistent Store Coordinator
    
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        
        let coordinator:NSPersistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let options = [NSMigratePersistentStoresAutomaticallyOption : true, NSInferMappingModelAutomaticallyOption : true]
        
        if let persistantStores = self.persistentStoreItems {
            
            for store in persistantStores {
                
                if let config = store.configuration {
                    
                    try! coordinator.addPersistentStoreWithType(NSInMemoryStoreType, configuration: config, URL: nil, options: options)
                    
                } else {
                    
                    try! coordinator.addPersistentStoreWithType(NSInMemoryStoreType, configuration: nil, URL: nil, options: options)
                }
            }
        }
        
        return coordinator
    }()
    
    // Managed Object Model
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        
        var managedObjectModel: NSManagedObjectModel!
        
        if let persistantStores = self.persistentStoreItems {
            
            var models: [NSManagedObjectModel] = [NSManagedObjectModel]()
            
            for store in persistantStores {
                
                let modelUrl = NSBundle.mainBundle().URLForResource(store.model, withExtension:"momd")
                let model = NSManagedObjectModel(contentsOfURL: modelUrl!)
                
                models.append(model!)
            }
            
            if models.count > 1 {
                
                managedObjectModel = NSManagedObjectModel(byMergingModels: models)
                
            } else {
                
                managedObjectModel = models[0]
            }
        }
        
        return managedObjectModel
    }()
    
    // MARK: Managed Object Contexts
    /*
     * Main object context is connected to persistent store (used to fetch data from storage to display in the app)
     * Background object context is connected to Main object context, used to fetch data in background. This is the use-forget kind of context
     */
    
    lazy var mainManagedObjectContext: NSManagedObjectContext = {
        
        let mainContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.MainQueueConcurrencyType)
        mainContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        
        return mainContext
    }()
    
    lazy var backgroundManagedObjectContext: NSManagedObjectContext = {
        
        let backgroundContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        backgroundContext.mergePolicy = NSOverwriteMergePolicy
        backgroundContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        
        return backgroundContext
    }()
    
    // MARK: Init
    
    override init() {
        super.init()
        
        //register for notification about changes in managed object context
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DatabaseManager.contextHasChangedNotification(_:)), name:  NSManagedObjectContextDidSaveNotification, object: nil)
    }
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // MARK: Merge context
    
    func contextHasChangedNotification(notification: NSNotification) {
        
        if let context = notification.object as? NSManagedObjectContext {
            
            //check if this is save from our context
            if context == self.backgroundManagedObjectContext {
                
                dispatch_async(dispatch_get_main_queue()) {
                    
                    //merge changes from background to main context
                    self.mainManagedObjectContext.mergeChangesFromContextDidSaveNotification(notification)
                }
            }
        }
    }
    
    // MARK: Helpers
    
    func setPersistentStoreItems(persistentStoreItems: [PersistentStoreCoreDataItem]?) {
        
        self.persistentStoreItems = persistentStoreItems
    }
}

class PersistentStoreCoreDataItem {
    
    private (set) var model: String
    private (set) var configuration: String?
    
    var fileName: String {
        
        if let config = self.configuration {
            
            return "\(self.model)_\(config)"
            
        } else {
            
            return self.model
        }
    }
    
    init(name:String) {
        
        self.model = name
    }
    
    init(name:String, configuration:String) {
        
        self.model = name
        self.configuration = configuration
    }
}