//
//  FromJSON.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData

protocol FromJSON {
    
    associatedtype T
    
    static func fromJSON(json:JSON, inContext context:NSManagedObjectContext) -> T?
}