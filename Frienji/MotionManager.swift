//
//  MotionManager.swift
//  Traces
//
//  Created by Adam Szeremeta on 15.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreMotion
import RxSwift
import RxCocoa

class MotionManager: NSObject {
    
    private let kInclinationsStorageCount = 10
    
    private let kMotionManagerSessionQueueIdentifier = "MotionManagerSessionQueue"
    
    // MARK: Shared instance
    
    static let sharedInstance = MotionManager()
    
    // MARK: Properties
    internal var motionManager:CMMotionManager!
    internal var motionManagerSessionQueue: NSOperationQueue!
    private (set) var isGatheringData:Bool = false

    let currentInclination = Variable<CGFloat>(0)
    private var inclinations = [Double]()
    
    // MARK: Init
    
    override init() {
        super.init()
        
        setUpMotionManager()
    }
    
    // MARK: Motion manager

    private func setUpMotionManager() {
        
        self.motionManager = CMMotionManager()
        self.motionManager.accelerometerUpdateInterval = 0.05
        
        self.motionManagerSessionQueue = NSOperationQueue()
    }
    
    func startMotionUpdates() {
                
        if self.motionManager.accelerometerAvailable {
            
            self.isGatheringData = true
            self.motionManager.startAccelerometerUpdatesToQueue(self.motionManagerSessionQueue) { (accelerometerData:CMAccelerometerData?, error:NSError?) in
                
                if let data = accelerometerData {
                    
                    var inclination:Double = 0
                    
                    let rollingY = data.acceleration.y
                    let rollingZ = data.acceleration.z
                    
                    if rollingZ > 0.0 {
                        
                        inclination = atan(rollingY/rollingZ) + M_PI/2.0
                        
                    } else if rollingZ < 0.0 {
                        
                        inclination = atan(rollingY/rollingZ) - M_PI/2.0
                        
                    } else if rollingY < 0.0 {
                        
                        inclination = M_PI/2.0
                        
                    } else if rollingY >= 0.0 {
                        
                        inclination = 3.0 * (M_PI/2.0)
                    }
                                        
                    self.inclinations.append(inclination)
                    if self.inclinations.count > self.kInclinationsStorageCount {
                        
                        self.inclinations.removeFirst()
                    }

                    let average = self.inclinations.reduce(0, combine: +) / Double(self.inclinations.count)
                    self.currentInclination.value = CGFloat(average)
                }
            }
        }
    }
    
    func stopMotionUpdates() {
        
        self.isGatheringData = false
        self.motionManager.stopAccelerometerUpdates()
    }
    
}