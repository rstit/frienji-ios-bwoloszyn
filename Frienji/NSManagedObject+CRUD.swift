//
//  NSManagedObject+CRUD.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreData

extension DatabaseEntity where Self: NSManagedObject {
    
    // MARK: DatabaseEntity
    
    static var entityName: String {
        
        return String(Self)
    }
    
    // MARK: Insert
    
    static func insertNewObjectForEntity(inContext context:NSManagedObjectContext) -> Self {
        
        return NSEntityDescription.insertNewObjectForEntityForName(Self.entityName, inManagedObjectContext: context) as! Self
    }
    
    static func insertOrGetObjectForEntity(entityDbId:AnyObject, keyId:String, inContext context:NSManagedObjectContext) -> Self {
        
        var object = Self.getEntityById(entityDbId, keyId: keyId, context: context)
        
        if object == nil {
            
            object = Self.insertNewObjectForEntity(inContext: context)
        }
        
        return object!
    }
    
    // MARK: Get
    
    static func getEntityById(entityDbId:AnyObject, keyId:String, context:NSManagedObjectContext) -> Self? {
        
        var object:Self? = nil
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName(entityName, inManagedObjectContext: context)
        
        let predicate = NSPredicate(format: "%K == %@", keyId, entityDbId as! NSObject)
        request.predicate = predicate
        
        let objectsArray = try? context.executeFetchRequest(request)
        object = objectsArray?.last as? Self
        
        return object
    }
    
    static func getEntities(sortKey:String?, predicate: NSPredicate?, ascending:Bool, context:NSManagedObjectContext) -> [Self] {
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName(Self.entityName, inManagedObjectContext: context)
        request.predicate = predicate
        
        if let key = sortKey {
            
            let sortDescriptor = NSSortDescriptor(key: key, ascending: ascending)
            request.sortDescriptors = [sortDescriptor];
        }
        
        do {
            
            return try context.executeFetchRequest(request) as! [Self]

        } catch (_) {
            
            return [Self]()
        }
    }
    
    static func getEntitiesByIds(ids:[NSNumber], sortKey:String?, ascending:Bool, context:NSManagedObjectContext) -> [Self] {
        
        let predicate = NSPredicate(format: "(dbID IN %@)", ids)

        return Self.getEntities(sortKey, predicate: predicate, ascending: ascending, context: context)
    }
    
    
}