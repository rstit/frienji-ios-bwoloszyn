//
//  Settings.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

class Settings {
    
    // MARK: Defaults Keys
    
    private struct DefaultsKeys
    {
        static let TracesAuthorizationData = "TracesAuthorizationDataKey"
        static let PushNotificationsToken = "PushNotificationsTokenKey"
        static let RegisteredForPushNotifications = "RegisteredForPushNotificationsKey"
        static let IsUserVerfied = "IsUserVerfied"

    }

    // MARK: Shared instance
    
    static let sharedInstance = Settings()
    
    // MARK: Properties
    
    var defaults: NSUserDefaults
    
    // MARk: Init
    
    init() {
        
        self.defaults = NSUserDefaults.standardUserDefaults()
    }
    
    // MARK: Settings

    var tracesAuthorizationData: [String: String]? {
        
        get {
            
            return self.defaults.objectForKey(DefaultsKeys.TracesAuthorizationData) as? [String: String]
        }
        
        set {
            
            self.defaults.setObject(newValue, forKey: DefaultsKeys.TracesAuthorizationData)
        }
    }
    
    var pushNotificationsToken:String? {
        
        get {
            
            return defaults.objectForKey(DefaultsKeys.PushNotificationsToken) as? String
        }
        
        set {
            
            defaults.setObject(newValue, forKey: DefaultsKeys.PushNotificationsToken)
        }
    }
    
    var registeredForPushNotifications: Bool {
        
        get {
            
            return defaults.objectForKey(DefaultsKeys.RegisteredForPushNotifications) as? Bool ?? false
            
        } set {
            
            defaults.setObject(newValue, forKey: DefaultsKeys.RegisteredForPushNotifications)
        }
    }
    
    var userVerified: Bool {
        get {
            return self.defaults.objectForKey(DefaultsKeys.IsUserVerfied) as? Bool ?? false
        } set {
            self.defaults.setObject(newValue, forKey: DefaultsKeys.IsUserVerfied)
        }
    }

}