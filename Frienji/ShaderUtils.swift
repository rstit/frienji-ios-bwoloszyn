//
//  ShaderUtils.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import AVFoundation
import OpenGLES

class ShaderUtils {
    
    class func compileShaderWithName(shaderName:String, shaderType: GLenum, bundle:NSBundle) -> GLuint? {
        
        // Tell OpenGL to create an OpenGL object to represent the shader, indicating if it's a vertex or a fragment shader.
        let shaderHandle: GLuint = glCreateShader(shaderType)
        
        do {
            
            if let shaderPath = bundle.pathForResource(shaderName, ofType: "glsl") {
                
                //load shared from file
                let shaderString = try NSString(contentsOfFile: shaderPath, encoding: NSUTF8StringEncoding)
                var shaderStringUTF8 = shaderString.UTF8String
                var shaderStringLength: GLint = Int32(shaderString.length)

                // Conver shader string to CString and call glShaderSource to give OpenGL the source for the shader.
                glShaderSource(shaderHandle, 1, &shaderStringUTF8, &shaderStringLength)

                // Tell OpenGL to compile the shader.
                glCompileShader(shaderHandle)
                
                // Check for errors
                var compileSuccess: GLint = GLint()
                glGetShaderiv(shaderHandle, UInt32(GL_COMPILE_STATUS), &compileSuccess)
                if (compileSuccess == GL_FALSE) {

                    fatalError("Unable to compile shader: \(shaderName)")
                }

                return shaderHandle
            }
            
            return nil
            
        } catch (_) {
            
            fatalError("Unable to compile shader: \(shaderName)")
        }
        
        return nil
    }
    
}