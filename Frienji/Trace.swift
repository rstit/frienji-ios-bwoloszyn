//
//  Trace.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

class Trace: NSManagedObject, DatabaseEntity {

    @NSManaged var dbID: Int64
    @NSManaged var latitude: Double
    @NSManaged var longitude: Double
    @NSManaged var hasPrize: Bool
    @NSManaged var logoImage: String?
    
    // MARK: Computed properties
    
    var location:CLLocation {
        
        return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
    
    var locationCoordinates:CLLocationCoordinate2D {
        
        return CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
    }
    
}
