//
//  TracesApi.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreData

class TracesApi {
    
    private enum Endpoint: String {
        
        case SignUp = "users"
        case SignIn = "users/sign_in"
    }
 
    // MARK: Shared Instance
    
    static let sharedInstance = TracesApi()

    // MARK: Properties
    
    private let kApiVersion = "v1/"
    
    internal var settings: Settings
    internal var context: NSManagedObjectContext
    
    // MARK: Init
    
    init() {
        
        self.settings = Settings.sharedInstance
        self.context = DatabaseHelper.sharedInstance.backgroundManagedObjectContext
    }

    // MARK: General request method
    
    internal func performRequest(method: Alamofire.Method, url: String, parameters: [String : AnyObject]?, encoding: Alamofire.ParameterEncoding, headers: [String : String]?, handler:((response:(Response<AnyObject, NSError>)) -> Void)?) {
        
        Alamofire.request(method, url, parameters: parameters, encoding: encoding, headers: headers).validate(statusCode: 200..<300).responseJSON { response in
            
            handler?(response: response)
        }
    }
    
    // MARK: Helpers
    
    private func createRequestPath(endpoint:Endpoint) -> String {
        
        let baseURL = ConfigurationsHelper.sharedInstance.getBackendUrl()
        
        return baseURL + "\(kApiVersion)\(endpoint.rawValue)"
    }
    
    // MARK: Authorization headers
    
    private func storeAuthorizationDataFromResponse(responseObject:AnyObject) {
        
        let kTokenJsonKey = "token"
        let kClientJsonKey = "client"
        
        let responseJson = JSON(responseObject)

        if let token = responseJson[kTokenJsonKey].string, let client = responseJson[kClientJsonKey].string {
        
            let autorizationData = [
                kTokenJsonKey: token,
                kClientJsonKey: client
            ]
            
            self.settings.tracesAuthorizationData = autorizationData
        }
    }
    
    private func createAuthorizationHeaders() -> [String: String] {
        
        if let autorizationData = self.settings.tracesAuthorizationData {
            
            return autorizationData
        }
        
        return [String: String]()
    }
    
    // MARK: Login / Sign up

    func signInWithCredentials(email:String, password:String, completionHandler:(() -> Void)?, errorHandler:((error:TracesApiError) -> Void)?) {
        
        let params = [
            "email": email,
            "password": password
        ]
        
        self.performRequest(.POST, url: createRequestPath(Endpoint.SignIn), parameters: params, encoding: .JSON, headers: nil) { (response) -> Void in
            
            switch response.result {
                
            case .Success(let responseObject):
                
                self.storeAuthorizationDataFromResponse(responseObject)
                
                completionHandler?()
                
            case .Failure(_):
                
                errorHandler?(error: TracesApiError.fromResponse(response, defaultMessage: nil))
            }
        }
    }

    func signUpWithCredentials(email:String, password:String, completionHandler:(() -> Void)?, errorHandler:((error:TracesApiError) -> Void)?) {
        
        let params = [
            "email": email,
            "password": password
        ]
        
        self.performRequest(.POST, url: createRequestPath(Endpoint.SignUp), parameters: params, encoding: .JSON, headers: nil) { (response) -> Void in
            
            switch response.result {
                
            case .Success(let responseObject):
                
                self.storeAuthorizationDataFromResponse(responseObject)
                
                completionHandler?()
                
            case .Failure(_):
                
                errorHandler?(error: TracesApiError.fromResponse(response, defaultMessage: nil))
            }
        }
    }

    
    
    
    
    
}