//
//  TracesApiError.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CFNetwork
import Alamofire

class TracesApiError {
    
    enum ErrorCodes : Int {
        
        case Unauthorized = 401
        
        
        static let messages = [
            
            401 : Localizations.api_error.unauthorized
        ]
    }
    
    // MARK: Properties
    
    private (set) var statusCode:Int?
    private (set) var errorCode:Int?
    var message:String = ""
    
    // MARK: Init
    
    init(statusCode:Int?, errorCode:Int?) {
        
        self.statusCode = statusCode
        self.errorCode = errorCode
        
        createNetworkErrorMessage()
        
        if self.message.isEmpty {
            
            createStatusCodeMessage()
        }
    }
    
    class func fromResponse(response: Response<AnyObject, NSError>, defaultMessage:String?) -> TracesApiError {
        
        var statusCode: Int? = nil
        var errorCode: Int? = nil
        
        switch response.result {
            
        case .Failure(let error):
            
            statusCode = response.response?.statusCode
            errorCode = error.code
            
        default:
            break
        }
        
        let error = TracesApiError(statusCode: statusCode, errorCode: errorCode)
        
        if defaultMessage != nil && error.message.isEmpty {
            
            error.message = defaultMessage!
        }
        
        if error.message.isEmpty {
            
            error.createDefaultErrorMessage()
        }
        
        return error
    }
    
    // MARK: Error message
    
    private func createNetworkErrorMessage() {
        
        if self.errorCode != nil && self.errorCode! == Int(CFNetworkErrors.CFURLErrorNotConnectedToInternet.rawValue) {
            
            self.message = Localizations.api_error.no_internet_connection
        }
    }
    
    private func createStatusCodeMessage() {
        
        if let code = self.statusCode, let message = ErrorCodes.messages[code] {
            
            self.message = message
        }
    }
    
    private func createDefaultErrorMessage() {
        
        if let code = self.statusCode {
            
            self.message = "\(Localizations.api_error.generic_message) \(code)"
            
        } else {
            
            self.message = "\(Localizations.api_error.generic_message)"
        }
    }

}