//
//  TransitionDelegate.swift
//  Frienji
//
//  Created by bolek on 27.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class TransitionDelegate: NSObject, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate
{
    static let sharedInstance :TransitionDelegate = TransitionDelegate();
//    static var pred :dispatch_once_t!;
    
    private var animationPresent :AnimatorPresent!;
    private var animationPush :AnimatorPush!;
    
    override init()
    {
        super.init()
        self.animationPresent = AnimatorPresent();
        self.animationPush = AnimatorPush();
    }
    
    //MARK: UINavigationControllerDelegate
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        if(operation == UINavigationControllerOperation.Push)
        {
            self.animationPush.isPresenting = true;
        }
        else if(operation == UINavigationControllerOperation.Pop)
        {
            self.animationPush.isPresenting = false;
        }
        return self.animationPush;
    }
    
    //MARK: UIViewControllerTransitioningDelegate
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        self.animationPresent.isPresenting = true;
        return self.animationPresent;
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        self.animationPresent.isPresenting = false;
        return self.animationPresent;
    }
}
