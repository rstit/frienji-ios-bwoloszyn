//
//  UIAlertUtils.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class UIAlertUtils {
    
    class func showAlertWithTitle(title:String, fromController:UIViewController, showCompletion:(() -> Void)?) {
        
        let alert = UIAlertController(title: title, message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: Localizations.alert.ok_button, style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            
            alert.dismissViewControllerAnimated(true, completion: nil)
        }
        
        alert.addAction(okAction)
        
        fromController.presentViewController(alert, animated: true, completion: showCompletion)
    }
    
    class func showAlertWithTitle(title:String, message:String, fromController:UIViewController, showCompletion:(() -> Void)?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: Localizations.alert.ok_button, style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            
            alert.dismissViewControllerAnimated(true, completion: nil)
        }
        
        alert.addAction(okAction)
        
        fromController.presentViewController(alert, animated: true, completion: showCompletion)
    }
    
    class func showAlertWithTitle(title:String, message:String, positiveButton:String, negativeButton:String, fromController:UIViewController, showCompletion:(() -> Void)?, actionHandler:((possitiveButtonTouched:Bool) -> Void)?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        let positiveAction = UIAlertAction(title: positiveButton, style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            
            actionHandler?(possitiveButtonTouched: true)
        }
        
        let negativeAction = UIAlertAction(title: negativeButton, style: UIAlertActionStyle.Cancel) { (action:UIAlertAction) -> Void in
            
            actionHandler?(possitiveButtonTouched: false)
        }
        
        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        
        fromController.presentViewController(alert, animated: true, completion: nil)
    }
    
}