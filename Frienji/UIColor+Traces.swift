//
//  UIColor+Traces.swift
//  Traces
//
//  Created by Adam Szeremeta on 22.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func mapBlueBlueColor() -> UIColor {
        
        return UIColor(red: 43.0 / 255.0, green: 165.0 / 255.0, blue: 175.0 / 255.0, alpha: 1.0)
    }
    
}