//
//  UIViewController+StoryboardLoad.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

protocol StoryboardLoad: class {
    
    /** The storyboard idendifier */
    static var storyboardId: String {get}
    
    /** The controller identifier in the storyboard */
    static var storyboardControllerId: String {get}
    
}

extension StoryboardLoad where Self: UIViewController {
    
    static var storyboardControllerId: String {
        
        return String(Self)
    }
    
    static func loadFromStoryboard() -> Self {
        
        let bundle = NSBundle(forClass: Self.self)
        let storyboard = UIStoryboard(name: Self.storyboardId, bundle: bundle)
        let vc = storyboard.instantiateViewControllerWithIdentifier(Self.storyboardControllerId) as! Self
        
        return vc
    }
    
}