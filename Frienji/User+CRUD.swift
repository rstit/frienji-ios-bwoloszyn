//
//  User+CRUD.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreData

extension User {
    
    class func getCurrentUser(inContext context:NSManagedObjectContext) -> User? {
        
        return User.getEntities(nil, predicate: nil, ascending: false, context: context).first
    }
    
}