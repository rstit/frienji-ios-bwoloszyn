//
//  User.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreData

class User: NSManagedObject, DatabaseEntity {

    @NSManaged var dbID: Int64
    @NSManaged var profilePicture: String?
    @NSManaged var email: String?
    @NSManaged var username: String?
}
