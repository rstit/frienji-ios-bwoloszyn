//
//  ArOpenGLViewTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 19.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Traces

class ArOpenGLViewTests: XCTestCase {
    
    var openGlView:ArOpenGLView!
    
    override func setUp() {
        super.setUp()
        
        self.openGlView = ArOpenGLView()
        self.openGlView.frame = CGRectMake(0, 0, 50, 50)
    }
    
    override func tearDown() {
        super.tearDown()
        
    }

    // MARK: Tests

    func testThatActivatingRendererChangesViewState() {
    
        //given
        XCTAssertFalse(self.openGlView.renderingActive, "Rendering should be disabled")
        
        //when
        self.openGlView.activateRenderer()
    
        //then
        XCTAssertTrue(self.openGlView.renderingActive, "Rendering should be enabled")
    }
    
    func testThatDeactivatingRendererChangesViewState() {
        
        //given
        self.openGlView.activateRenderer()
        XCTAssertTrue(self.openGlView.renderingActive, "Rendering should be enabled")

        //when
        self.openGlView.deactivateRenderer()
        
        //then
        XCTAssertFalse(self.openGlView.renderingActive, "Rendering should be disabled")
    }
}