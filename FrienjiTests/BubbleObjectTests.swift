//
//  BubbleObjectTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 19.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
import CoreData
@testable import Traces

class BubbleObjectTests: XCTestCase {
    
    var context:NSManagedObjectContext!
    
    override func setUp() {
        super.setUp()
     
        self.context = DatabaseTestContext.managedObjectContextForTests()
    }
    
    override func tearDown() {
        super.tearDown()
        
    }

    // MARK: Tests
    
    func testThatBubbleSizeIsCalculatedAndSaved() {
        
        //given
        let trace = Trace.insertNewObjectForEntity(inContext: self.context)
        
        let bubble = BubbleObject(trace: trace)
        XCTAssertEqual(bubble.bubbleSize.width, 0)

        //when
        bubble.calculateBubbleSizeForDesiredSize(50, viewportWidth: 320, viewportHeight: 568, animated: false)

        //then
        XCTAssertNotEqual(bubble.bubbleSize.width, 0, "Size should be calculated")
    }
    
    func testThatBubblePositionIsCalculatedAndSaved() {
        
        //given
        let trace = Trace.insertNewObjectForEntity(inContext: self.context)

        let bubble = BubbleObject(trace: trace)
        bubble.calculateBubbleSizeForDesiredSize(50, viewportWidth: 320, viewportHeight: 568, animated: false)
        XCTAssertEqual(bubble.bubblePosition.x, 0)
        
        //when
        bubble.calculateBubblePositionForBearing(30, minFieldOfViewAngle: 15, maxFieldOfViewAngle: 45)
        
        //then
        XCTAssertNotEqual(bubble.bubblePosition.x, 0, "Position should be calculated")
    }
    
    func testThatBubbleYPositionIsCalculatedAndSaved() {
        
        //given
        let trace = Trace.insertNewObjectForEntity(inContext: self.context)

        let bubble = BubbleObject(trace: trace)
        bubble.calculateBubbleSizeForDesiredSize(50, viewportWidth: 320, viewportHeight: 568, animated: false)
        XCTAssertEqual(bubble.bubblePosition.y, 0)
        
        //when
        bubble.calculateBubblePositionForYMotion(1.1)
        
        //then
        XCTAssertNotEqual(bubble.bubblePosition.y, 0, "Position should be calculated")
    }

}