//
//  FacebookHelperTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 21.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Traces

class FacebookHelperTests : XCTestCase {
    
    var facebookHelper:FakeFacebookHelper!
    
    override func setUp() {
        super.setUp()
        
        self.facebookHelper = FakeFacebookHelper()
    }
    
    override func tearDown() {
        super.tearDown()
        
    }

    // MARK: Tests

    func testThatFacebookHelperCallsErrorHandlerInCaseOfLoginError() {
        
        let readyExpectation = expectationWithDescription("ready")
        
        //given
        (self.facebookHelper.loginManager as? FakeFBSDKLoginManager)?.error = NSError(domain: "test", code: 0, userInfo: nil)
        
        let controller = UIViewController()
        UIApplication.sharedApplication().keyWindow?.rootViewController = controller
        
        //when
        self.facebookHelper.authenticateWithFacebook(fromController: controller, completionHandler: { (user) in
            
            
        }) { (error) in
                
            readyExpectation.fulfill()
        }
        
        //then
        waitForExpectationsWithTimeout(3, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }

    func testThatFacebookHelperCallsCompletionHandlerIfThereWasNoError() {
        
        let readyExpectation = expectationWithDescription("ready")
        
        //given
        self.facebookHelper.userData = FacebookUser(id: "1", email: "aa@aa.aa", name: "test")
        
        let controller = UIViewController()
        UIApplication.sharedApplication().keyWindow?.rootViewController = controller
        
        //when
        //when
        self.facebookHelper.authenticateWithFacebook(fromController: controller, completionHandler: { (user) in
            
            readyExpectation.fulfill()
            
        }) { (error) in
            
        }
        
        //then
        waitForExpectationsWithTimeout(3, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }

}