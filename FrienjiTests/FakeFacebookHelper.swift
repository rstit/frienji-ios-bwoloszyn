//
//  FakeFacebookHelper.swift
//  Traces
//
//  Created by Adam Szeremeta on 21.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit
@testable import Traces

class FakeFacebookHelper : FacebookHelper {
    
    var userData:FacebookUser?
    
    override init() {
        super.init()
        
        self.loginManager = FakeFBSDKLoginManager()
    }
    
    override func isUserSessionActive() -> Bool {
        
        return true
    }
    
    override func getLoggedUserProfile(completionHandler:((user:FacebookUser) -> Void), errorHandler:((error:String?) -> Void)) {
        
        self.userData != nil ? completionHandler(user: self.userData!) : errorHandler(error: nil)
    }

}

class FakeFBSDKLoginManager : FBSDKLoginManager {
    
    var error:NSError?
    
    override func logInWithReadPermissions(permissions: [AnyObject]!, fromViewController: UIViewController!, handler: FBSDKLoginManagerRequestTokenHandler!) {
        
        let token = FBSDKAccessToken(tokenString: "a", permissions: [AnyObject](), declinedPermissions: [AnyObject](), appID: "123", userID: "123", expirationDate: NSDate(), refreshDate: NSDate())
        let loginResult = FBSDKLoginManagerLoginResult(token: token, isCancelled: false, grantedPermissions: Set<NSObject>(), declinedPermissions: Set<NSObject>())
        
        handler(loginResult, self.error)
    }

}