//
//  FakeLocationManager.swift
//  Traces
//
//  Created by Adam Szeremeta on 19.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreLocation
@testable import Traces

class FakeLocationManager : LocationManager {
    
    override init() {
        super.init()
        
        self.locationManager = FakeCLLocationManager()
        self.locationManager.delegate = self
    }
    
}

private class FakeCLLocationManager : CLLocationManager {
    
    override func startUpdatingLocation() {
        
        let location = CLLocation(latitude: 51.1, longitude: 23.3)
        self.delegate?.locationManager!(self, didUpdateLocations: [location])
    }
    
    override func startUpdatingHeading() {
        
        let heading = CLHeading()
        self.delegate?.locationManager!(self, didUpdateHeading: heading)
    }
    
    private override func requestWhenInUseAuthorization() {
        
    }
    
}