//
//  FakeSetings.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
@testable import Traces

class FakeSettings : Settings {
    
    override init() {
        super.init()
        
        self.defaults = DefaultsFake()
    }
    
}

private class DefaultsFake : NSUserDefaults {
    
    private var storage = [String: AnyObject]()
    
    // MARK: Override
    
    override func objectForKey(defaultName: String) -> AnyObject? {
        
        return self.storage[defaultName]
    }
    
    override func setObject(value: AnyObject?, forKey defaultName: String) {
        
        self.storage[defaultName] = value
    }
}