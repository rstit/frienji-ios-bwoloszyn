//
//  FakeTracesApi.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Alamofire
import CoreData
@testable import Traces

struct FakeResponse {
    
    var data: NSHTTPURLResponse?
    var jsonData: AnyObject?
}

class FakeTracesApi : TracesApi {
    
    var request: String?
    var response = FakeResponse(data: nil, jsonData: nil)
    
    // MARK: Init
    
    override init() {
        super.init()
        
        self.settings = FakeSettings()
        self.context = DatabaseTestContext.managedObjectContextForTests()
    }
    
    // MARK: Override
    
    override func performRequest(method: Alamofire.Method, url: String, parameters: [String : AnyObject]?, encoding: Alamofire.ParameterEncoding, headers: [String : String]?, handler: ((response: (Response<AnyObject, NSError>)) -> Void)?) {
        
        let result = self.response.jsonData != nil ?
            Result<AnyObject, NSError>.Success(self.response.jsonData!) :
            Result<AnyObject, NSError>.Failure(NSError(domain: "", code: -1, userInfo: nil))
        
        let fakeResponse = Response(request: NSURLRequest(URL: NSURL(string:self.request!)!), response: self.response.data, data: nil, result: result)
        
        handler?(response: fakeResponse)
    }
}