//
//  FakeTwitterHelper.swift
//  Traces
//
//  Created by Adam Szeremeta on 21.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import TwitterKit
@testable import Traces

class FakeTwitterHelper : TwitterHelper {
    
    var userData:TwitterUser?
    
    override init() {
        super.init()
        
        self.twitterClient = FakeTwitter.sharedInstance()
    }
    
    override func isUserSessionActive() -> Bool {
        
        return true
    }
    
    override func getEmailForCurrentlyLoggedUser(completionHandler:((user:TwitterUser) -> Void), errorHandler:((error:String?) -> Void)) {
    
        self.userData != nil ? completionHandler(user: self.userData!) : errorHandler(error: nil)
    }
}

class FakeTwitter : Twitter {
    
    var error:NSError?
    
    override func logInWithViewController(controller:UIViewController?, methods: TWTRLoginMethod, completion: TWTRLogInCompletion) {
        
        completion(nil, self.error)
    }
}