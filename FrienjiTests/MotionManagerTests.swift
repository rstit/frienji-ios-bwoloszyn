//
//  MotionManagerTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 19.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
import RxSwift
import RxCocoa
@testable import Traces

class MotionManagerTests: XCTestCase {
    
    var motionManager:FakeMotionManager!
    
    override func setUp() {
        super.setUp()
        
        self.motionManager = FakeMotionManager()
    }
    
    override func tearDown() {
        super.tearDown()
        
    }

    // MARK: Tests

    func testThatMotionManagerGathersDataAfterStart() {
        
        XCTAssertFalse(self.motionManager.isGatheringData, "Should not gather data")
        
        //when
        self.motionManager.startMotionUpdates()
        
        //then
        XCTAssertTrue(self.motionManager.isGatheringData, "Should gather data")
    }
    
    func testThatAfterStartingUpdatesInclinationIsCalculated() {
        
        //given
        let readyExpectation = expectationWithDescription("ready")
        let disposeBag = DisposeBag()
        
        XCTAssertEqual(0, self.motionManager.currentInclination.value, "Should be zero")
        
        //when
        self.motionManager.currentInclination.asObservable().bindNext { (inclination:CGFloat) in
            
            //then
            readyExpectation.fulfill()
            
        }.addDisposableTo(disposeBag)
        
        self.motionManager.startMotionUpdates()
        
        waitForExpectationsWithTimeout(3, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    func testThatMotionManagerDoesNotGatherDataAfterStop() {
        
        //given
        self.motionManager.startMotionUpdates()
        XCTAssertTrue(self.motionManager.isGatheringData, "Should gather data")

        //when
        self.motionManager.stopMotionUpdates()
        
        //then
        XCTAssertFalse(self.motionManager.isGatheringData, "Should not gather data")
        
    }
}