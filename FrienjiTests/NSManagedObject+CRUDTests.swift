//
//  NSManagedObject+CRUDTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
import CoreData
@testable import Traces

class NSManagedObject_CRUDTests : XCTestCase {

    var context:NSManagedObjectContext!
    
    override func setUp() {
        super.setUp()
        
        self.context = DatabaseTestContext.managedObjectContextForTests()
    }
    
    override func tearDown() {
        super.tearDown()
        
    }
    
    // MARK: Tests
    // Test generic CRUD methods using Trace entity
    
    func testCreatingNewEntityObject() {
        
        //when
        let entity = Trace.insertNewObjectForEntity(inContext: self.context)
        
        XCTAssertNotNil(entity, "Entity should be created")
    }
    
    func testThatInsertingExistingObjectReturnsThatObject() {
        
        //given
        let entity = Trace.insertNewObjectForEntity(inContext: self.context)
        entity.dbID = 1
        
        //when
        let fetchedEntity = Trace.insertOrGetObjectForEntity(NSNumber(longLong: entity.dbID), keyId: "dbID", inContext: self.context)
        
        //then
        XCTAssertEqual(entity.dbID, fetchedEntity.dbID, "Should be the same")
    }
    
    func testGettingAllEntities() {
        
        //given
        let entity = Trace.insertNewObjectForEntity(inContext: self.context)
        entity.dbID = 1
        
        let entity2 = Trace.insertNewObjectForEntity(inContext: self.context)
        entity2.dbID = 2
        
        //when
        let entities = Trace.getEntities(nil, predicate: nil, ascending: false, context: self.context)
        
        //then
        XCTAssertEqual(entities.count, 2, "Two entities should be returned")
    }
    
    func testGettingEntitiesByIds() {
        
        //given
        let entity = Trace.insertNewObjectForEntity(inContext: self.context)
        entity.dbID = 1
        
        let entity2 = Trace.insertNewObjectForEntity(inContext: self.context)
        entity2.dbID = 2

        let entity3 = Trace.insertNewObjectForEntity(inContext: self.context)
        entity3.dbID = 3

        let ids = [NSNumber(longLong: entity.dbID), NSNumber(longLong: entity3.dbID)]
        
        //when
        let entities = Trace.getEntitiesByIds(ids, sortKey: nil, ascending: false, context: self.context)
        
        //then
        XCTAssertEqual(entities.count, 2, "Two entities should be returned")
    }
    
}