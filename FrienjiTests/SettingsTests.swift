//
//  SettingsTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Traces

class SettingsTests : XCTestCase {
    
    var settings:FakeSettings!
    
    override func setUp() {
        super.setUp()
        
        self.settings = FakeSettings()
    }
    
    override func tearDown() {
        super.tearDown()
        
        self.settings = nil
    }

    // MARK: Tests

    func testSavingAuthorizationData() {
        
        //given
        let data = [
            "token": "aasjflaf",
            "client": "12345"
        ]
        XCTAssertNil(self.settings.tracesAuthorizationData, "Data should be not present")
        
        //when
        self.settings.tracesAuthorizationData = data
        
        //then
        XCTAssertEqual(data, self.settings.tracesAuthorizationData!, "Data should match")
    }

    func testSavingPushNotificationsToken() {
        
        //given
        let token = "asdjhalf"
        XCTAssertNil(self.settings.pushNotificationsToken, "Token should not be present")
        
        //when
        self.settings.pushNotificationsToken = token
        
        //then
        XCTAssertEqual(token, self.settings.pushNotificationsToken, "Token should match")
    }
    
    func testSavingRegisteredForPushNotifications() {
        
        //given
        var registered = false
        XCTAssertEqual(registered, self.settings.registeredForPushNotifications, "Should not be registered")

        //when
        registered = true
        self.settings.registeredForPushNotifications = registered
        
        //then
        XCTAssertEqual(registered, self.settings.registeredForPushNotifications, "Should be registered")
    }
    
}