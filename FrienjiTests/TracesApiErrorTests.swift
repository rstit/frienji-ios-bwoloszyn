//
//  TracesApiErrorTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Traces

class HypeitApiErrorTests : XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        super.tearDown()
        
    }
    
    // MARK: Tests
    
    func testThatErrorMessageIsCreatedWhenInitializingErrorWithCodes() {
        
        //given
        let statusCode = 401
        let errorCode = 1000
        
        //when
        let error = TracesApiError(statusCode: statusCode, errorCode: errorCode)
        
        //then
        XCTAssertGreaterThan(error.message.characters.count, 0, "Message should not be empty")
    }
}