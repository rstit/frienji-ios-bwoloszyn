//
//  TracesApiTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Traces

class TracesApiTests: XCTestCase {
    
    var api:FakeTracesApi!
    
    override func setUp() {
        super.setUp()
        
        self.api = FakeTracesApi()
    }
    
    override func tearDown() {
        super.tearDown()
        
        self.api = nil
    }

    // MARK: Tests (Sign in)
    
    func testThatSignInCallsCompletionHandlerForPossitiveApiCallback() {
        
        //given
        let expectation = expectationWithDescription("api")
        
        let responseDict = [
            "status": "success",
            "token": "K2z4zclhT-OfRCaivbb8rQ",
            "client": "CegrvXyotyDMeA8YBNwJ_g",
            "uid": "adamszeremeta@rst-it.com"
        ]
        
        self.api.request = "users/sign_in"
        self.api.response.data = NSHTTPURLResponse(URL: NSURL(string: self.api.request!)!, statusCode: 200, HTTPVersion: "HTTP/1.1", headerFields: nil)
        self.api.response.jsonData = responseDict
        
        //when
        self.api.signInWithCredentials("adamszeremeta@rst-it.com", password: "abc", completionHandler: { () -> Void in
            
            expectation.fulfill()
            
        }) { (error:TracesApiError) -> Void in
            
            XCTAssertTrue(false, "Wrong block called!")
        }
        
        //then
        waitForExpectationsWithTimeout(3) { (error:NSError?) -> Void in
            
        }
    }
    
    func testThatSignInCallsErrorHandlerForBadApiResponse() {
        
        //given
        let expectation = expectationWithDescription("api")
        
        self.api.request = "users/sign_in"
        self.api.response.data = NSHTTPURLResponse(URL: NSURL(string: self.api.request!)!, statusCode: 401, HTTPVersion: "HTTP/1.1", headerFields: nil)
        
        //when
        self.api.signInWithCredentials("adamszeremeta@rst-it.com", password: "abc", completionHandler: { () -> Void in
            
            XCTAssertTrue(false, "Wrong block called!")
            
        }) { (error:TracesApiError) -> Void in
            
            expectation.fulfill()
        }
        
        //then
        waitForExpectationsWithTimeout(3) { (error:NSError?) -> Void in
            
        }
    }

    // MARK: Tests (Sign up)
    
    func testThatSignUpCallsCompletionHandlerForPossitiveApiCallback() {
        
        //given
        let expectation = expectationWithDescription("api")
        
        let responseDict = [
            "status": "success",
            "token": "K2z4zclhT-OfRCaivbb8rQ",
            "client": "CegrvXyotyDMeA8YBNwJ_g",
            "uid": "adamszeremeta@rst-it.com"
        ]
        
        self.api.request = "users"
        self.api.response.data = NSHTTPURLResponse(URL: NSURL(string: self.api.request!)!, statusCode: 201, HTTPVersion: "HTTP/1.1", headerFields: nil)
        self.api.response.jsonData = responseDict
        
        //when
        self.api.signInWithCredentials("adamszeremeta@rst-it.com", password: "abc", completionHandler: { () -> Void in
            
            expectation.fulfill()
            
        }) { (error:TracesApiError) -> Void in
            
            XCTAssertTrue(false, "Wrong block called!")
        }
        
        //then
        waitForExpectationsWithTimeout(3) { (error:NSError?) -> Void in
            
        }
    }
    
    func testThatSignIPCallsErrorHandlerForBadApiResponse() {
        
        //given
        let expectation = expectationWithDescription("api")
        
        self.api.request = "users"
        self.api.response.data = NSHTTPURLResponse(URL: NSURL(string: self.api.request!)!, statusCode: 400, HTTPVersion: "HTTP/1.1", headerFields: nil)
        
        //when
        self.api.signInWithCredentials("adamszeremeta@rst-it.com", password: "abc", completionHandler: { () -> Void in
            
            XCTAssertTrue(false, "Wrong block called!")
            
        }) { (error:TracesApiError) -> Void in
            
            expectation.fulfill()
        }
        
        //then
        waitForExpectationsWithTimeout(3) { (error:NSError?) -> Void in
            
        }
    }
    
    
}