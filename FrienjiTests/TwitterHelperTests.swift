//
//  TwitterHelperTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 21.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Traces

class TwitterHelperTests : XCTestCase {
    
    var twitterHelper:FakeTwitterHelper!
    
    override func setUp() {
        super.setUp()
        
        self.twitterHelper = FakeTwitterHelper()
    }
    
    override func tearDown() {
        super.tearDown()
        
    }

    // MARK: Tests

    func testThatTwitterHelperCallsErrorHandlerInCaseOfLoginError() {
        
        let readyExpectation = expectationWithDescription("ready")
        
        //given
        (self.twitterHelper.twitterClient as? FakeTwitter)?.error = NSError(domain: "test", code: 0, userInfo: nil)
        let controller = UIViewController()
        UIApplication.sharedApplication().keyWindow?.rootViewController = controller
        
        //when
        self.twitterHelper.authenticateWithTwitter(fromController: controller, completionHandler: { (user) in
            
            
        }) { (error) in
                
            readyExpectation.fulfill()
        }
        
        //then
        waitForExpectationsWithTimeout(3, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    func testThatTwitterHelperCallsCompletionHandlerIfThereWasNoError() {
        
        let readyExpectation = expectationWithDescription("ready")

        //given
        self.twitterHelper.userData = TwitterUser(id: "123", email: "aa@aa.aa", name: "Test")
        
        let controller = UIViewController()
        UIApplication.sharedApplication().keyWindow?.rootViewController = controller
        
        //when
        self.twitterHelper.authenticateWithTwitter(fromController: controller, completionHandler: { (user) in
            
            readyExpectation.fulfill()
            
        }) { (error) in
            
        }

        //then
        waitForExpectationsWithTimeout(3, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
}