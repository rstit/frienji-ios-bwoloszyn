//
//  User+CRUDTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
import CoreData
@testable import Traces

class User_CRUDTests : XCTestCase {
    
    var context:NSManagedObjectContext!
    
    override func setUp() {
        super.setUp()
        
        self.context = DatabaseTestContext.managedObjectContextForTests()
    }
    
    override func tearDown() {
        super.tearDown()
        
    }

    // MARK: Tests

    func testGettingCurrentUser() {
        
        //given
        let user = User.insertNewObjectForEntity(inContext: self.context)
        user.dbID = 1
        
        //when
        let fetchedUser = User.getCurrentUser(inContext: self.context)
        
        //then
        XCTAssertEqual(user.dbID, fetchedUser?.dbID, "User should be fetched")
    }
}