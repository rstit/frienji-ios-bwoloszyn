# Traces

This is basic source of information about Traces iOS application. Application supports iOS versions 8.x and higher and is designed only for iPhone devices.

- - -

Content of file:
====================

* [General](#features)
    * [Branches](#markdown-header-branches)
    * [Dependencies](#markdown-header-dependencies)
    * [App structure](#markdown-header-app-structure)
    * [Project structure](#markdown-header-project-structure)
* [Functionalities](#markdown-header-branches)
    * [AR Functionality](#markdown-header-ar-functionality)

- - -

## Branches:

* master - stable app releases
* develop - main flow of the application, here are merged features from feature branches 

## Dependencies:

All dependencies are provided using CocoaPods. If for some reason dependency can't be added as pod, it will be in Library folder as source files.

## App structure:

MVVM

## Project structure:

* Resources - fonts, strings, images etc.
* UI - all storyboards
* Sources:
    * Api - all classes responsible for communication with backend or other services
    * Model - all model related classes and entities
    * Views - all custom view classes used globally in app
    * Extensions - all class extensions
    * Utils - different classes which are providing some utility to the project
    * Modules:
        Here are project files like controllers etc. divided into groups by functionality, not by type
* Supporting Files - config files etc.

# Functionalities

- - -

## AR Functionality

Augmented Reallity functionality is written in OpenGL ES 2.0. It's used to "catch" Trace. Traces are represented by floating glassy bubbles around the user. User have to point his device to Trace he wants to catch. Below you can find technical details of this functionality and how it's implemented.
    
- - -

Camera

Application captures camera video feed on separate thread and saves it to the buffer. That data is then used to create still image / per frame which is used as OpenGL texture. It's then used to draw two trangles filling whole screen using texture from camera frame. That is base for AR, live camera feed.

- - -

Traces (bubbles)

Each Trace object is drawn used simple two trangles structure with camera texture created step earlier. Glassy look is created by modifing source texture pixels and adding lighting affects to it in the fragment shader. In this shader we are also discarding pixels which are not part of the bubble (sphere/circle). 

Trace size is calculated using real distance between user position and location assigned to Trace object. This can be described by pseudocode below:
    
    define min and max trace size (in pixels) realtive to screen size
    calculate distance
    normalize distance to fit in our min/max range
    calculate trace size (pixels)
    transform pixel size to OpenGL screen size (rectangle (-1,-1), (1,1))
    
Trace horizontal position is calculated using compas readings and camera FOV (field of view defined in degrees). This can be described by pseudocode below:

    get compas true heading
    define mix/max screen heading
        min = heading - FOV/2 - some maring
        max = heading + FOV/2 + some margin
    calculate trace bearing between user location and trace location
    if bearing is between our min/max range
        calculate OpenGL screen position and mark object as valid to be rendered
        apply low pass filter to make smooth transition between old and new position
    else
        mark as not valid to be rendered

Trace vertical position is calculated using gyroscope & accelerometer readings. This can be described by pseudocode below:

    define starting vertical position as half of the screen
    get accelerometer data
    calculate average reading from last n measurements
    calculate vertical device tilt
    add tilt value to starting position (this will move trace up or down)
    apply low pass filter to smooth transition between old and new position

- - -

Renderer

Below are the renderer steps used to draw every frame

    set viewport size
    set orthographic projection matrix
    clear render buffer
    bind program to draw camera feed
    draw camera frame
    bind program to draw bubbles
    for trace in traces
        draw trace
    present render buffer

